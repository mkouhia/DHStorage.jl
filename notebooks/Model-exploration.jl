
using DHStorage, Plotly, TimeZones, CPLEX, DataFrames
cd("C:/Users/mkouhia/Documents/DHStorage.jl")

t0 = ZonedDateTime(DateTime("2017-01-01"), TimeZone("Europe/Helsinki"))
t1 = ZonedDateTime(DateTime("2017-12-31T23"), TimeZone("Europe/Helsinki"))
args = DHStorage.simulation_arguments
_m, r_opt  = DHStorage.solve_sliding_window(t0, t1; args...,)
_m, r_base = DHStorage.solve_sliding_window(t0, t1; args..., baseline=true);

plotheat(r_opt)

plotheat(r_base)

plotu = collect(eachindex(DHStorage.units))[vec([sum(r_opt[:Qₛ] .+ r_base[:Qₛ], 1)' .≠ 0]...)]
traces = GenericTrace[]
for u in plotu, rp in [("opt",r_opt), ("ref",r_base)] 
    t = scatter(; x = 1:8760, y = sort(vec(rp[2][:Qₛ][:,u]), rev=true), mode="lines",
        marker_color = DHStorage.unit_colors[u],
        line_dash = rp[1] == "opt" ? "solid" : "dash",
        name="$(DHStorage.units[u].name): $(rp[1])")
    push!(traces, t)
end
plot(traces, Layout(; xaxis_title="Time /h", yaxis_title="Power Qₛ /MW"))

Qtopt, Qtbase = [sum(r[:Qₛ],1) .* 1e-3 for r in  [r_opt, r_base]]
println("Heat generation per unit, GWh/a")
@printf("%15s %6s %6s\n", "Unit", "Opt", "Ref")
for u in eachindex(DHStorage.units)
    @printf("%15s %6.1f %6.1f\n", DHStorage.units[u].name, Qtopt[u], Qtbase[u])
end

qsupp = sum(r_base[:Qₛ],2)
pm = 0.85 * maximum(qsupp)
@printf("Base: %d\nOpt : %d\n", sum(sum(r_base[:Qₛ],2) .> pm), sum(sum(r_opt[:Qₛ],2) .> pm))

pm

plott(r_opt)

plott(r_base)

@printf("%40s %6s %6s\n", "", "Opt", "Base")
@printf("%40s %6.2f %6.2f\n", "Average heat production cost, EUR/MWh",
    map(ri -> sum(ri[:Cᵥ]) + sum(ri[:Cₓ]) + sum(ri[:C_p]), [r_opt, r_base]) ./ sum(r_base[:heatload])...)
@printf("%40s %6.2f %6.2f\n", "Heat losses, % of input heat",
    map(ri -> 100 * (1 - sum(ri[:heatload]) / sum(ri[:Qₛ])), [r_opt, r_base])...)
@printf("%40s %6.2f %6.2f\n", "Heat rejection, % of input heat",
    map(ri -> 100 * (sum(ri[:Qᵣ]) / sum(ri[:Qₛ])), [r_opt, r_base])...)
@printf("%40s %6.2f %6.2f\n", "Pumping power, % of input heat",
    map(ri -> 100 * (sum(ri[:Pp]) / sum(ri[:Qₛ])), [r_opt, r_base])...)

plot(
    [
        scattergl(;x=vec(r_opt[:temp]), y=vec(r_opt[:Tₛ′]), mode="markers", name="opt"),
        scattergl(;x=vec(r_base[:temp]), y=vec(r_base[:Tₛ′]), mode="markers", name="ref"),
    ],
    Layout(;xaxis_title = "Ambient temperature /°C", yaxis_title = "DH supply temperature /°C",)
)

plot(
    [
        scattergl(;x=vec(r_opt[:temp]), y=vec(sum(r_opt[:Qₛ], 2)), mode="markers", name="opt"),
        scattergl(;x=vec(r_base[:temp]), y=vec(sum(r_base[:Qₛ], 2)), mode="markers", name="ref"),
    ],
    Layout(;xaxis_title = "Ambient temperature /°C", yaxis_title = "DH supply /MW",)
)

Qx(r) = DHStorage.cₚ * (0.5 * DHStorage.ρ * args[:V_dhn]) * args[:kₓ] * (r[:Tₛ] + r[:Tᵣ] - 2 * r[:temp] ) / (3600 * DHStorage.Δτ)
plot(
    [
        scattergl(; x=vec(r_opt[:temp]), y=vec(Qx(r_opt) .* DHStorage.dhcost(r_opt)), mode="markers", name="Cₓ"),
        scattergl(; x=vec(r_opt[:temp]), y=vec(r_opt[:C_p]), mode="markers", name="Cₚ"),
    ],
        Layout(;xaxis_title = "Ambient temperature /°C", yaxis_title = "Cost of energy consumption /€",)

)

Qx(r) = DHStorage.cₚ * (0.5 * DHStorage.ρ * args[:V_dhn]) * args[:kₓ] * (r[:Tₛ] + r[:Tᵣ] - 2 * r[:temp] ) / (3600 * DHStorage.Δτ)
plot(
    [
        scattergl(; x=vec(r_base[:temp]), y=vec(Qx(r_base) .* DHStorage.dhcost(r_base)), mode="markers", name="Cₓ"),
        scattergl(; x=vec(r_base[:temp]), y=vec(r_base[:C_p]), mode="markers", name="Cₚ"),
    ],
        Layout(;xaxis_title = "Ambient temperature /°C", yaxis_title = "Cost of energy consumption /€",)

)

plot(
    [
        scattergl(; x=vec(r_opt[:temp]), y=vec(r_opt[:ṁ]), mode="markers", name="opt"),
        scattergl(; x=vec(r_base[:temp]), y=vec(r_base[:ṁ]), mode="markers", name="ref"),
    ],
    Layout(;xaxis_title = "Ambient temperature /°C", yaxis_title = "DH mass flow rate /kg s⁻¹",)
)

plot(
    [
        scattergl(;x=1:8760, y=r_opt[:Qᵣ][:,1], name="opt"),
        scattergl(;x=1:8760, y=sort(r_opt[:Qᵣ][:,1], rev=true), name="opt, LDR"),
        scattergl(;x=1:8760, y=r_base[:Qᵣ][:,1], name="base"),
        scattergl(;x=1:8760, y=sort(r_base[:Qᵣ][:,1], rev=true), name="base, LDR"),
    ],
    Layout(;title="CHP heat rejection")
)

plot(
    [scattergl(; x = vec(r_opt[:Qₛ][:,1] + r_opt[:Qᵣ][:,1]), y=vec(r_opt[:Pₒ][:,1]), mode="markers")],
    Layout(;xaxis_title = "Qₛ + Qᵣ /MW", yaxis_title = "Pₒ /MW", title="CHP power vs heat load")
)

plot([scattergl(;y=vec(DHStorage.dhcost(r_base)), x=vec(r_base[:Qₛ][:,u]), mode="markers", name=DHStorage.units[u].name) for u in 1:6])

plot([scattergl(;y=vec(DHStorage.dhcost(r_opt)), x=vec(r_opt[:Qₛ][:,u]), mode="markers", name=DHStorage.units[u].name) for u in 1:6])

plot([scattergl(;y=DHStorage.dhcost(r_base))])

r_base[:elcost][5458]

plot([
        scattergl(;x=1:8760, y=sort(DHStorage.dhcost(r_base), rev=true), name="Base"),
        scattergl(;x=1:8760, y=sort(DHStorage.dhcost(r_opt), rev=true), name="Opt"),
    ]
)
