
using PlotlyJS, CSV, DataFrames, DHStorage
cd("C:/Users/mkouhia/Documents/DHStorage.jl")

df = CSV.read("elcost_sen.csv")

function plot_elec(plotsym)
    assert(plotsym in [:ec_scale :ec_var])
    dd = df[ isa.(df[plotsym], Float64) , :]
    lines = map([true, false]) do tf
        dx = dd[dd[:baseline] .== tf,:]
        scatter(; y = dx[:dhcost], x = dx[plotsym], name = tf ? "ref" : "opt")
    end
    k = by(dd, plotsym) do d
        i_ref, i_opt = map(_tf -> findfirst(d[:baseline], _tf), [true, false])
        DataFrame(opt_diff = (1. - d[i_opt, :dhcost] / d[i_ref, :dhcost]) * 100.)
    end
    !isempty(k) && push!(lines, scatter(; x=k[plotsym], y=k[:opt_diff],
            yaxis="y2", line_color=DHStorage.unit_colors[8], name = "diff"))
    plot(
        lines,
        Layout(
            yaxis_title = "Average DH cost /€ MWh⁻¹",
            xaxis_title = @sprintf("Electricity cost %s factor", plotsym == :ec_scale ? "scaling" : "variance"),
            yaxis2 = attr(
                title = "Opt/ref improvement /%",
                titlefont = attr(color = DHStorage.unit_colors[8]),
                tickfont = attr(color = DHStorage.unit_colors[8]),
                overlaying = "y",
                side = "right",
                showgrid = false,
            )
        )
    )
end

plot_elec(:ec_scale)

plot_elec(:ec_var)

function plot_elec(df)
    lines = PlotlyBase.GenericTrace[]
    for (i, plotsym) in enumerate([:ec_scale :ec_var])
        dd = df[ isa.(df[plotsym], Float64) , :]
        ldash = plotsym == :ec_scale ? "solid" : "dash"
        appname(s) = (plotsym == :ec_scale ? "scaling" : "variance") * ": " * s
        for (j, tf) in enumerate([true, false])
            dx = dd[dd[:baseline] .== tf,:]
            push!(lines, scatter(; y = dx[:dhcost], x = dx[plotsym],
                    name = appname(tf ? "ref" : "opt"), line_dash = ldash,
                    legendgroup = i,
                    line_color = DHStorage.unit_colors[j]))
        end
        k = by(dd, plotsym) do d
            i_ref, i_opt = map(_tf -> findfirst(d[:baseline], _tf), [true, false])
            DataFrame(opt_diff = (1. - d[i_opt, :dhcost] / d[i_ref, :dhcost]) * 100.)
        end
        !isempty(k) && push!(lines, scatter(; x=k[plotsym], y=k[:opt_diff],
                yaxis="y2", line_color=DHStorage.unit_colors[8], name = appname("diff"),
                legendgroup = i, line_dash = ldash))
    end
    plot(
        lines,
        Layout(
            yaxis_title = "Average DH cost /€ MWh⁻¹",
            xaxis_title = "Electricity cost factor",
            yaxis2 = attr(
                title = "Opt/ref improvement /%",
                titlefont = attr(color = DHStorage.unit_colors[8]),
                tickfont = attr(color = DHStorage.unit_colors[8]),
                overlaying = "y",
                side = "right",
                showgrid = false,
            ),
            legend_orientation = "h",
        )
    )
end

plot_elec(df)
