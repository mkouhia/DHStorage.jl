
using DHStorage, CPLEX, PlotlyJS, TimeZones
# init_notebook(true)
cd("C:/Users/mkouhia/Documents/DHStorage.jl")

function get_temp_load_elcost(time, hours)
    temp     = DHStorage.predict_temp(  time, hours)
    heatload = DHStorage.predict_demand(time, hours)
    elcost   = DHStorage.predict_elcost(time, hours)
    return temp, heatload, elcost
end

args = DHStorage.simulation_arguments

t0 = ZonedDateTime(DateTime("2017-01-01"), TimeZone("Europe/Helsinki")).utc_datetime
t1 = ZonedDateTime(DateTime("2017-01-14"), TimeZone("Europe/Helsinki")).utc_datetime;

res = []
wws = [24, 36, 48, 72, 96, 120]
times = Float64[]
for ww in wws
    _t = @elapsed m, r = DHStorage.solve_sliding_window(t0, t1; args..., window_width = ww, verbose=0)
    push!(res, r)
    push!(times, _t)
end

times

xi = 1:length(res[1][:heatload])
plot([scatter(; x = xi, y = vec(res[i][:Tₛ]), mode="lines", name=string(wws[i])) for i in eachindex(wws)])

function show_diff(sym)
    @printf("%4s %10s %s\n", "WW", "RMSE", "Mean abs. error %")
    for i in 1:length(wws)-1
        @printf("%4d %10.2e %.2f\n", wws[i],
            sum((res[i][sym] .- res[end][sym]).^2 ./ length(xi))^(1/2) ,
            mean(abs.((res[i][sym] .- res[end][sym]) ./ res[end][sym] )) * 100
        )
    end
end
function show_diff(sym, index)
    @printf("%4s %10s %s\n", "WW", "RMSE", "Mean abs. error %")
    for i in 1:length(wws)-1
        @printf("%4d %10.2e %.2f\n", wws[i],
            sum((res[i][sym][:, index] .- res[end][sym][:, index]).^2 ./ length(xi))^(1/2) ,
            mean(abs.((res[i][sym][:, index] .- res[end][sym][:, index]) ./ res[end][sym][:, index] )) * 100
        )
    end
end

show_diff(:Tₛ)

plot([scatter(; x = xi, y = vec(res[i][:Qₛ][:,1]), mode="lines", name=string(wws[i])) for i in eachindex(wws)])

show_diff(:Qₛ, 1)

t2 = ZonedDateTime(DateTime("2017-07-01"), TimeZone("Europe/Helsinki")).utc_datetime
t3 = ZonedDateTime(DateTime("2017-07-14"), TimeZone("Europe/Helsinki")).utc_datetime
res2 = []
for ww in wws
    m, r = DHStorage.solve_sliding_window(t2, t3; args..., window_width = ww, baseline=true, verbose=0)
    push!(res2, r)
end
xi = 1:length(res2[1][:heatload])
plot([scatter(; x = xi, y = vec(res2[i][:Tₛ]), mode="lines", name=string(wws[i])) for i in eachindex(wws)])

t70 = ZonedDateTime(DateTime("2017-07-01"), TimeZone("Europe/Helsinki")).utc_datetime
t71 = ZonedDateTime(DateTime("2017-07-30"), TimeZone("Europe/Helsinki")).utc_datetime
res2 = []
wwlts = [7, 14, 30, 45, 60] .* 24
for ww in wwlts
    m, r = DHStorage.solve_sliding_window(t70, t71; args...,
        window_width = 48, window_width_lt = ww, baseline = true, verbose=0)
    push!(res2, r)
end

xi2 = 1:length(res2[1][:heatload])
plot([scatter(; x = t70+Dates.Hour.(xi), y = vec(res2[i][:Qₛ][:,1]), mode="lines", name=string(wwlts[i]/24))
        for i in eachindex(wwlts)])

[sum(r[:C_s]) for r in res2]

totalcost(ri) = sum(ri[:Cᵥ]) + sum(ri[:Cₓ]) + sum(ri[:C_p])
for i in eachindex(res2)
    @printf("%2.0f %8.2e\n", wwlts[i]/24, totalcost(res2[i]))
end
