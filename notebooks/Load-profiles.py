
# coding: utf-8

# In[1]:


import demandlib.bdew
import workalendar
import os
import pandas as pd


import plotly.offline as py
import plotly.graph_objs as go

import sys
sys.path.insert(0, "C:/Users/mkouhia/Documents/DHStorage.jl")
import scripts.prepare_data as prep


# In[2]:


d = prep.avg_temperature(*[os.path.join(os.pardir, i) for i in prep.fmi_weather_files])

# Remove TZ information, while keeping the actual times in UTC+2
d.index = d.index.tz_localize(None)

years = set(d.index.year)
cal = workalendar.europe.Finland()
_hd = []
for y in years:
    _hd.extend(cal.holidays(y))
holidays = dict(_hd)

# Simulated demand
# NOTE parameter annual_heat_demand is only for scaling normalized output,
# and it will be the sum of heat consumption OVER THE WHOLE INDEX.
dem_2017 = 332.9e6 # kWh [Energiateollisuus vuositaulukot 2017]
building_types = ["MFH", "EFH", "GHD"]
for typ in building_types:
    key = "demand" + typ
    d[key] = demandlib.bdew.HeatBuilding(
        d.index,
        holidays = holidays,
        temperature = d['temp.C'],
        shlp_type = typ,  # Mehrfamilienhaus = block of flats
        building_class = 1 if typ != "GHD" else 0, # Seems proper
        wind_class = 0,     # Not windy
        annual_heat_demand = 100e6, # kWh [Hast2017]
        ww_incl = True,     # Warm water included
        name = 'Aggregated buildings').get_bdew_profile() / 1000
    # Do proper scaling
    scaler = dem_2017 * 1e-3 / sum(d[d.index.year == 2017][key])
    d[key] = scaler * d[key]

d["demand.MWh"] = 0.4 * d["demandMFH"] + 0.3 * d["demandEFH"] + 0.3 * d["demandGHD"]


# In[3]:


di = d[(d.index >= "2017-01-01") & (d.index <= "2017-01-07")]
data = [go.Scatter( x=di.index, y=di[i], name=i) for i in ["demand.MWh", "demandMFH", "demandEFH", "demandGHD"]]
py.init_notebook_mode(connected=True)
py.iplot(data, filename = 'basic-line')


# In[4]:


di2 = d[(d.index >= "2017-07-01") & (d.index <= "2017-07-07")]
data = [go.Scatter( x=di.index, y=di2[i], name=i) for i in ["demand.MWh", "demandMFH", "demandEFH", "demandGHD"]]
py.iplot(data, filename = 'basic-line')


# In[5]:


d[d.index.year == 2017].sum()


# In[6]:


d[d.index.year == 2017].max()


# In[7]:


d[d.index.year == 2017].min()


# In[8]:


di = d[d.index.year == 2017]
data = [go.Scatter( x=list(range(1,8761)), y=di.sort_values(by=i, ascending=False)[i], name=i) for i in ["demand.MWh", "demandMFH"]]#, "demandEFH", "demandGHD"]]
py.init_notebook_mode(connected=True)
py.iplot(data, filename = 'basic-line')


# # Result
# 
# Move away from using "MFH" profile only. Instead use combination, as outlined above.
