
using DHStorage, Plotly, TimeZones, CPLEX, DataFrames
cd("C:/Users/mkouhia/Documents/DHStorage.jl")

t0 = ZonedDateTime(DateTime("2017-01-01"), TimeZone("Europe/Helsinki"))
t1 = ZonedDateTime(DateTime("2017-12-31T23"), TimeZone("Europe/Helsinki"))
args = DHStorage.simulation_arguments;

sen_Qmax_opt = Dict{}()
sen_Qmax_base = Dict{}()

fqd = []
qmax_list = [0., 10., 20., 30.]
for qmax_hp in qmax_list
    push!(fqd, Dict(5 => qmax_hp, 6 => 0.))
end
push!(fqd, Dict(5 => 0., 6 => 10.))

for fq in fqd
    m, r = DHStorage.solve_sliding_window(t0, t1; args..., fix_Qmax = fq)
    sen_Qmax_opt[fq] = r
    m, r = DHStorage.solve_sliding_window(t0, t1; args..., fix_Qmax = fq, baseline = true)
    sen_Qmax_base[fq] = r
end

plotheat(sen_Qmax_opt[Dict(5=>0.,6=>0.)])

plotheat(sen_Qmax_opt[Dict(5=>10.,6=>0.)])

plotheat(sen_Qmax_opt[Dict(5=>20.,6=>0.)])

p0 = plotheat(sen_Qmax_opt[Dict(5 =>  0., 6 => 0.)])
p0.plot.layout["xaxis_range"]=[1050, 1350]
p0

p1 = plotheat(sen_Qmax_opt[Dict(5 => 10., 6 => 0.)])
p1.plot.layout["xaxis_range"]=[1050, 1350]
p1

p2 = plotheat(sen_Qmax_opt[Dict(5 => 20., 6 => 0.)])
p2.plot.layout["xaxis_range"]=[1050, 1350]
p2

p3 = plotheat(sen_Qmax_opt[Dict(5 => 30., 6 => 0.)])
p3.plot.layout["xaxis_range"]=[1050, 1350]
p3

_r = sen_Qmax_opt[Dict(5=>20.,6=>0.)]
p1 = plott(_r)
p1.plot.layout["xaxis_range"]=[1050, 1350]
p2 = plot([scatter(;x=1050:1350, y=_r[:elcost][1050:1350], name="elcost")])
display([p1, p2])

function _plot_sen(xl, opt, base, xaxis_title)
    plot([
            scatter(; x = xl, y = map(v -> DHStorage.dhcost_avg(opt[v]), xl),
                mode="lines+markers", name="opt"),
            scatter(; x = xl, y = map(v -> DHStorage.dhcost_avg(base[v]), xl),
                mode="lines+markers", name="ref"),
            scatter(; x = xl, y = map(v ->
                    (1 - DHStorage.dhcost_avg(opt[v]) / DHStorage.dhcost_avg(base[v])) * 100., xl),
                yaxis="y2", line_color=DHStorage.unit_colors[8], mode="lines+markers", name = "diff")

        ],
        Layout(;
            xaxis_title = xaxis_title, yaxis_title = "Average DH cost /€ MWh⁻¹",
            yaxis2 = attr(
                title = "Opt/ref improvement /%",
                titlefont = attr(color = DHStorage.unit_colors[8]),
                tickfont = attr(color = DHStorage.unit_colors[8]),
                overlaying = "y",
                side = "right",
                showgrid = false,
            )

        )
    )
end

sel_hp(d) = Dict(q => d[Dict(5 => q, 6 => 0.)] for q in qmax_list)


_plot_sen(qmax_list, sel_hp(sen_Qmax_opt), sel_hp(sen_Qmax_base), "HP installed power, MW")

function plot_LDR(case::String, unit::Int64 = 1)
    assert(case in ["opt", "base"])
    sen_dic = case == "opt" ? sen_Qmax_opt : sen_Qmax_base
    plot(
        map(cap -> scattergl(;
                x = 1:8760,
                y = sort(vec(sen_dic[Dict(5=>cap,6=>0.0)][:Qₛ][:, unit]), rev=true),
                mode="lines", name=string(cap)),
            qmax_list
        ),
        Layout(; title = "$(DHStorage.units[unit].name) load duration curve in case '$(case)': effect of HP size")
    )
end
plot_LDR("opt", 1)

plot_LDR("base", 1)

plot_LDR("opt", 5)

plot_LDR("base", 5)

function _plot_ldr_hp(rdhp::Dict)
    traces = GenericTrace[]
    v = .!all(isapprox.(vcat([r[:Qₛ] for r in values(rdhp)]...), 0; atol=1e-6), 1)
    plotu = collect(eachindex(DHStorage.units))[vec(v)]
    traces = GenericTrace[]
    for (i, qhp) in enumerate(sort(collect(keys(rdhp)))), u in plotu
        r = rdhp[qhp]
        t = scatter(; x = 1:8760, y = sort(vec(r[:Qₛ][:,u]), rev=true), mode="lines",
            marker_color = DHStorage.unit_colors[u],
            line_dash = ["solid", "dash", "dot", "dashdot"][i],
            name = @sprintf("%s: %.0f MW HP", DHStorage.units[u].name, qhp),
            legendgroup = u,
        )
        push!(traces, t)
    end
    p = plot(traces, Layout(; xaxis_title="Time /h", yaxis_title="Power Qₛ /MW"))
end

plot_ldr_hp(Dict(q => sen_Qmax_opt[Dict(5 => q, 6 => 0.)] for q in [0., 10., 20]))

plot_ldr_hp(Dict(q => sen_Qmax_base[Dict(5 => q, 6 => 0.)] for q in [0., 10., 20]))

function plot_ldr(r_opt, r_base; verbose = 1)
    plotu = collect(eachindex(DHStorage.units))[vec([sum(r_opt[:Qₛ] .+ r_base[:Qₛ], 1)' .≠ 0]...)]
    traces = GenericTrace[]
    for u in plotu, rp in [("opt",r_opt), ("ref",r_base)]
        all(isapprox.(rp[2][:Qₛ][:, u], 0.; atol=1e-6)) && continue
        t = scatter(; x = 1:8760, y = sort(vec(rp[2][:Qₛ][:,u]), rev=true), mode="lines",
            marker_color = DHStorage.unit_colors[u],
            line_dash = rp[1] == "opt" ? "solid" : "dash",
            name="$(DHStorage.units[u].name): $(rp[1])")
        push!(traces, t)
    end
    p = plot(traces, Layout(; xaxis_title="Time /h", yaxis_title="Power Qₛ /MW"))
end
plot_ldr(sen_Qmax_opt[Dict(5 => 10., 6 => 0.)], sen_Qmax_base[Dict(5 => 10., 6 => 0.)])

plotheat(sen_Qmax_opt[Dict(5=>0.,6=>10.)])

qi = sen_Qmax_opt[Dict(5=>0.,6=>10.)][:Qₛ]
sum(qi, 1)

@printf("%4s %30s %6s | %4s %4s %4s %4s %4s %4s\n", "ind", "time", "elcost", "CHP", "HFO", "NG", "BIO", "HP", "EB")
for ind in indices(qi,1)[qi[:,6] .!= 0]
    c_el = sen_Qmax_opt[Dict(5=>0.,6=>10.)][:elcost][ind]
    time = t0 + Dates.Hour(ind)
    @printf("%4d %30s %6.2f | %4.1f %4.1f %4.1f %4.1f %4.1f %4.1f\n", ind, time, c_el, qi[ind,:]...)
end
