
using DHStorage, Plotly, TimeZones, CPLEX, DataFrames
cd("C:/Users/mkouhia/Documents/DHStorage.jl")

t0 = ZonedDateTime(DateTime("2017-01-01"), TimeZone("Europe/Helsinki"))
t1 = ZonedDateTime(DateTime("2017-12-31T23"), TimeZone("Europe/Helsinki"))
args = DHStorage.simulation_arguments

sen_dtsmax  = Dict{}()
base_dtsmax = Dict{}()
dt_list = collect(linspace(3., 11., 5))
for dtsmax in dt_list
    m, r = DHStorage.solve_sliding_window(t0, t1; args..., ΔTₛ′max = dtsmax)
    sen_dtsmax[dtsmax] = r
    m, r = DHStorage.solve_sliding_window(t0, t1; args..., ΔTₛ′max = dtsmax, baseline = true)
    base_dtsmax[dtsmax] = r
end

function _plot_sen(xl, opt, base, xaxis_title)
    plot([
            scatter(; x = xl, y = map(v -> DHStorage.dhcost_avg(opt[v]), xl),
                mode="lines+markers", name="opt"),
            scatter(; x = xl, y = map(v -> DHStorage.dhcost_avg(base[v]), xl),
                mode="lines+markers", name="ref"),
            scatter(; x = xl, y = map(v ->
                    (1 - DHStorage.dhcost_avg(opt[v]) / DHStorage.dhcost_avg(base[v])) * 100., xl),
                yaxis="y2", line_color=DHStorage.unit_colors[8], mode="lines+markers", name = "diff")

        ],
        Layout(;
            xaxis_title = xaxis_title, yaxis_title = "Average DH cost /€ MWh⁻¹",
            yaxis2 = attr(
                title = "Opt/ref improvement /%",
                titlefont = attr(color = DHStorage.unit_colors[8]),
                tickfont = attr(color = DHStorage.unit_colors[8]),
                overlaying = "y",
                side = "right",
                showgrid = false,
            )

        )
    )
end

_plot_sen(dt_list, sen_dtsmax, base_dtsmax, "ΔTₛ′max /°C")
