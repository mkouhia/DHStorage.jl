
using DHStorage, Plotly, TimeZones, CPLEX, DataFrames
cd("C:/Users/mkouhia/Documents/DHStorage.jl")

t0 = ZonedDateTime(DateTime("2017-01-01"), TimeZone("Europe/Helsinki"))
t1 = ZonedDateTime(DateTime("2017-12-31T23"), TimeZone("Europe/Helsinki"))
args = DHStorage.simulation_arguments;

function plot_chp_eff(xl, opt, base, xaxis_title)
    plot([
            scatter(; x = xl, y = map(v -> DHStorage.dhcost_avg(opt[v]), xl),
                mode="lines+markers", name="opt"),
            scatter(; x = xl, y = map(v -> DHStorage.dhcost_avg(base[v]), xl),
                mode="lines+markers", name="ref"),
            scatter(; x = xl, y = map(v ->
                    (1 - DHStorage.dhcost_avg(opt[v]) / DHStorage.dhcost_avg(base[v])) * 100., xl),
                yaxis="y2", line_color=DHStorage.unit_colors[8], mode="lines+markers", name = "diff")

        ],
        Layout(;
            xaxis_title = xaxis_title, yaxis_title = "Average DH cost /€ MWh⁻¹",
            yaxis2 = attr(
                title = "Opt/ref improvement /%",
                titlefont = attr(color = DHStorage.unit_colors[8]),
                tickfont = attr(color = DHStorage.unit_colors[8]),
                overlaying = "y",
                side = "right",
                showgrid = false,
            )

        )
    )
end

opt_q  = Dict{}()
base_q = Dict{}()

# CHP plant size
chp_qfacs = collect(0.6:0.2:1.4)
for chp_qfac in chp_qfacs, bl in [true, false]
    units = copy(DHStorage.units)
    _q = units[1].Qₛmax
    u1 = units[1]
    units[1] = DHStorage.Unit([_f == :Qₛmax ? _q * chp_qfac : getfield(u1, _f) for _f in fieldnames(u1)]...)
    
    m, r = DHStorage.solve_sliding_window(t0, t1; args..., allunits = units, baseline = bl)
    if bl
        base_q[chp_qfac] = r
    else
        opt_q[chp_qfac] = r
    end
end

plot_chp_eff(chp_qfacs, opt_q, base_q, "CHP maximum DH power /MW")

opt_eff  = Dict{}()
base_eff = Dict{}()

# CHP efficiency
chp_effs = collect(linspace(1.1, 0.85, 6))
for chp_eff in chp_effs, bl in [true, false]
    units = copy(DHStorage.units)
    u1 = units[1]
    units[1] = DHStorage.Unit([_f == :ηₜ ? chp_eff : getfield(u1, _f) for _f in fieldnames(u1)]...)
    
    m, r = DHStorage.solve_sliding_window(t0, t1; args..., allunits = units, baseline = bl)
    if bl
        base_eff[chp_eff] = r
    else
        opt_eff[chp_eff] = r
    end
end

plot_chp_eff(chp_effs, opt_eff, base_eff, "CHP efficiency")

opt_suc  = Dict{}()
base_suc = Dict{}()

# CHP efficiency
chp_sucs = collect(500.:1000.:4500.)
for suc in chp_sucs, bl in [true, false]
    units = copy(DHStorage.units)
    u1 = units[1]
    units[1] = DHStorage.Unit([_f == :SC ? suc : getfield(u1, _f) for _f in fieldnames(u1)]...)
    
    m, r = DHStorage.solve_sliding_window(t0, t1; args..., allunits = units, baseline = bl)
    if bl
        base_suc[suc] = r
    else
        opt_suc[suc] = r
    end
end

plot_chp_eff(chp_sucs, opt_suc, base_suc, "CHP start-up costs /€")
