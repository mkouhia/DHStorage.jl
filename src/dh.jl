using JuMP
using Cbc
using Ipopt

using .Utils


"Heat or power generating unit"
struct Unit
    name::String
    kind::String
    Qₛmax::Float64  # Maximum district heat output (MW)
    Pmax::Float64   # Max electricity output (MW)
    Lmin::Float64   # Minimum load, share of electricity output
    ηₜ::Float64     # Total efficiency
    COP::Float64    # Heat pump coefficient of performance
    fuel::String    # Fuel
    SC::Float64     # Start-up cost (EUR)
    Cᵢ::Float64     # Investment cost (EUR). 0 implies unit existence.
    Cᵢ′::Float64    # Specific investment cost (EUR/MW DH)
    Com::Float64    # Variable operation and maintenance costs, EUR/(MWh DH)
    Cof::Float64    # Fixed operation and maintenance costs, EUR/(MW DH)/a
end


# Initial data

"All units available for optimization"
units = [
    #    name           kind Qₛmax Pmax Lmin  ηₜ   COP  fuel               SC     Cᵢ    Cᵢ′   Com   Cof
    Unit("BioCHP",     "CHP",  63,  22, 0.4, 1.10, NaN, "Wood fuel",      2500.,    0.,   NaN, 3.9, 29e3 ),
    Unit("HFO-HOB",    "HOB",  24, NaN, NaN, 0.85, NaN, "Heavy fuel oil",    0.,    0.,   NaN, 0. , 3700.),
    Unit("NG-HOBs",    "HOB", 126, NaN, NaN, 0.85, NaN, "Natural gas",       0.,    0.,   NaN, 0. , 3700.),
#   Unit("JarvenpaaH", "HOB",  40, NaN, NaN, 0.85, NaN, "Natural gas",       0.,    0.,   NaN, 0. ,    0.),
#   Unit("Ristinummi", "HOB",  45, NaN, NaN, 0.85, NaN, "Natural gas",       0.,    0.,   NaN, 0. ,    0.),
#   Unit("Tuusula",    "HOB",  15, NaN, NaN, 0.85, NaN, "Natural gas",       0.,    0.,   NaN, 0. ,    0.),
#   Unit("Sula",       "HOB",  15, NaN, NaN, 0.85, NaN, "Natural gas",       0.,    0.,   NaN, 0. ,    0.),
#   Unit("Kellokoski", "HOB",   9, NaN, NaN, 0.85, NaN, "Natural gas",       0.,    0.,   NaN, 0. ,    0.),
#   Unit("Lahela",     "HOB", 2.5, NaN, NaN, 0.85, NaN, "Natural gas",       0.,    0.,   NaN, 0. ,    0.),
    Unit("Bio-HOB",    "HOB",  18, NaN, NaN, 0.85, NaN, "Biomass/waste",     0.,    0.,   NaN, 5.4,    0.),
    Unit("HP",         "HP",  NaN, NaN, NaN,  NaN, 3.0, "NA",                0.,   NaN, 680e3, 0. , 5500.),
    Unit("EB",         "EB",  NaN, NaN, NaN, 1.00, NaN, "NA",                0.,   NaN,  80e3, 0.5, 1100.),
]
# # References:
# - EB & HP investment cost: \cite{energistyrelsen2012data}


"Fuel costs EUR/MWh (LHV)"
fuelcost = Dict{String, Float64}(
    "Wood fuel"      => 21.5,
    "Heavy fuel oil" => 35,
    "Natural gas"    => 27.5,
    "Biomass/waste"  => 35,
    "NA"             => 0,
)

"Additional fuel tax EUR/MWh (LHV)"
fueltax = Dict{String, Float64}(
    "Wood fuel"      => 0,
    "Heavy fuel oil" => 22,
    "Natural gas"    => 17.4,
    "Biomass/waste"  => 0,
    "NA"             => 0,
)


const datafile          = joinpath(@__DIR__, "..", "data", "processed", "optimization_data.csv")
const datafile_fallback = joinpath(@__DIR__, "..", "data", "processed", "optimization_data_sample.csv")

if isfile(datafile)
    _f = datafile
elseif isfile(datafile_fallback)
    warn("Data file $(datafile) is not accessible. Falling back to sample file $(datafile_fallback).")
    _f = datafile_fallback
else
    error("Data file $(datafile) and sample data file $(datafile_fallback) are inaccessible. Exiting.")
end

times, temperature, heatload, elcost = Utils.readdata(_f)
times_utc =  map(s -> s.utc_datetime, times)

"Big M for heat load"
const Mq = maximum(heatload) * 10

"Tolerance"
const tol = 1e-8
"Model time resolution (hours)"
const Δτ = 1

# DH network parameters, affecting DH storage constraint
"Specific heat capacity of water (MJ/kg/K)"
const cₚ = 4.2e-3
"Density of DH water (kg/m3)"
const ρ = 1e3


"""
    supplytemperature(Ta; Ta0=5.0, Ta1=-25.0, Ts0=75.0, Ts1=115.0)

Calculate district heat supply temperature based on ambient temperature `Ta`.

Employ adjustment curve where supply temperature is linearly adjusted between
district heat minimum and maximum supply temperatures `Ts0` and `Ts1` when
ambient temperature `Ta` is between minimum and maximum regulation temperatures
`Ta1` and `Ta0`. Otherwise, the temperature is fixed to the minimum or
maximum value.

# Examples
```jldoctest
julia> map(s -> supplytemperature(s; Ta0=5.0, Ta1=-30.0, Ts0=75.0, Ts1=120.0), [10.0, 5.0, 0.0, -15.0, -30.0, -35.0])
6-element Array{Float64,1}:
  75.0
  75.0
  81.4286
 100.714
 120.0
 120.0
```
"""
function supplytemperature(Ta::Float64;
        Ta0::Float64=5.0,  Ta1::Float64=-25.0,
        Ts0::Float64=75.0, Ts1::Float64=115.0,
    )
    if Ta >= Ta0
        return Ts0
    elseif Ta >= Ta1
        return Ts0 + (Ts1 - Ts0) * (Ta - Ta0) / (Ta1 - Ta0)
    else
        return Ts1
    end
end


"""
    distributioncost(dt, normalcost, winterdaycost; <keyword arguments>)

Calculate electricity distribution cost at given datetime.
Higher price on winter days.

# Arguments
- `normalcost::Float64=7.8`:    Normal cost, EUR/MWh
- `winterdaycost::Float64=17.5`:Winter day cost, EUR/MWh
- `wintermonths=[1,2,12]`:      Winter months
- `winterdays=1:5`:             Winter days
- `winterdayhours=7:21`:        Hours during winter days during which higher
                                price is payed
"""
function distributioncost(dt, normalcost::Float64=7.8, winterdaycost::Float64=17.5;
    wintermonths=[1,2,12],
    winterdays=1:5,
    winterdayhours=7:21)
    if month(dt) in wintermonths &&
            Dates.dayofweek(dt) in winterdays &&
            hour(dt) in winterdayhours
        return winterdaycost
    else
        return normalcost
    end
end



"""
    generateDHmodel(;<keyword arguments>)

Generate district heat JuMP model

Make a DH optimization problem, where DH supply and return temperatures
are allowed to vary, or a 'baseline' problem, where supply temperature
is set according to ambient temperature `Tₐ`.

# Arguments
-  `mip_solver = CbcSolver()`:      Solver for MIP model (longterm)
-  `nlp_solver = IpoptSolver()`:    Solver for NLP model

- `allunits::Array{Unit,1}`:             Units available in optimization
- `c_fuel::Dict{String,Float64}`:       Fuel costs (EUR/MWh,LHV)
- `c_fueltax::Dict{String,Float64}`:    Additional fuel tax (EUR/MWh,LHV)

- 'Qₗ::Array{Float64, 1}':      Heat load, hourly resolution (MWh/h)
- `c_el::Array{Float64, 1}`:    Electricity cost, hourly resolution (€/MWh)
- `Tₐ::Array{Float64, 1}`:      Ambient temperature, hourly resolution (°C)
- `c_ed::Array{Float64, 1}`:    Electricity distribution cost (€/MWh)

- `c_et=27.9372`:   Electricity tax (€/MWh)
- `c_ecb=2604.`:    Electricity connection base cost (€/year)
- `c_ecp=4.56e3*12`:Electricity connection power cost (€/MW/a)
- `Tₛmin=68`:       Minimum average supply temperature (in non-baseline case)
- `Tₛ′min=50`:      Minimum supply temperature (in non-baseline case)
- `Tₛ′max=115`:     Maximum supply temperature
- `Tret=45`:        Return temperature from the substations
- `ΔTₛ′max=15`:     Maximum temperature change rate in DHN (degC/h)
- `V_dhn=5356`:     Volume of water in district heating network (m3)
- `kₓ=5e-2`:        District heat network heat loss coefficient (1/period)
- `invrate=0.08`:   Investment rate per year
- `invtime::Integer=20`:    Investment time, years

- `Tₛ0::Float64=NaN`:  Starting value for `Tₛ` at index 0
- `Tₛ′0::Float64=NaN`: Starting value for `Tₛ′` at index 0
- `Tᵣ0::Float64=NaN`:  Starting value for `Tᵣ` at index 0
- `z0::Array{Int64,1}=Array{Int64,1}()`: Starting value for `z` at index 0

- `Tsm::Float64=NaN`:   Mean supply temperature Tₛ, for longterm heat loss calculation.
- `Trm::Float64=NaN`:   Mean return temperature Tᵣ, for longterm heat loss calculation.

- `fix_z::Array{Int64,2}`: Fixed integer values for `z`
- `strict_z::Bool`:     If true, force all z to fix_z values. Otherwise force
  only those units whose SC != 0 or Lmin is not NaN.

- `oppoint::Tuple{Float64, Float64}`: Network operating point. (Flow rate [kg/s], Δp [Pa])
- `ηp::Float64=0.7`:    Pump mechanical efficiency
- `ηm::Float64=0.9`:    Pump electric efficiency

- `baseline::Bool=false`: Solve for 'baseline' solution (see above)
- `fix_Qmax::Dict{Int64, Float64}=Dict{Int64,Float64}()`: Fixed maximum power (MW)
  for scalable units.

- `longterm::Bool = false`: Long term model: exclude DH water temperature,
  plan production assuming production equals consumption prediction.
  Otherwise, have a nonlinear model.

Setting `fix_Qmax[u]` to some value fixes `model[:Qmax][u] = value` and also
`zₑ[u] = 1`. If `Qmax[u] == 0`, set `zₑ[u] = 0`.
"""
function generateDHmodel(;
        mip_solver = CbcSolver(),
        nlp_solver = IpoptSolver(print_level=0),
        allunits::Array{Unit, 1}         = units,
        c_fuel::Dict{String, Float64}    = fuelcost,
        c_fueltax::Dict{String, Float64} = fueltax,
        Qₗ::Array{Float64, 1}   = heatload,
        c_el::Array{Float64, 1} = elcost,
        Tₐ::Array{Float64, 1}   = temperature,
        c_ed::Array{Float64, 1} = fill(7.8, length(c_el)),# \cite{hsv2018siirto}
        c_et::Float64   = 27.9372,          # \cite{hsv2018siirto}
        c_ecb::Float64  = 12 * 217.0,       # \cite{hsv2018siirto}
        c_ecp::Float64  = 4.56e3 * 12,      # €/MW/a
        Tₛmin::Float64  =  68.,
        Tₛ′min::Float64 =  50.,
        Tₛ′max::Float64 = 115.,
        Tret::Float64   =  45.,
        ΔTₛ′max::Float64=  15.,
        V_dhn::Float64  = 5356.,
        kₓ::Float64     = 1e-2,
        invrate::Float64 = 0.08, invtime::Int64 = 20,
        baseline = false,
        Tₛ0::Float64    = NaN,
        Tₛ′0::Float64   = NaN,
        Tᵣ0::Float64    = NaN,
        Tsm::Float64    = NaN,
        Trm::Float64    = NaN,
        fix_z::Array{Int64,2} = Array{Int64,2}(0, 0),
        strict_z::Bool = false,
        z0::Array{Int64,1} = Array{Int64,1}(),
        fix_Qmax::Dict{Int64, Float64} = Dict{Int64,Float64}(),
        longterm::Bool = false,
        oppoint::Tuple{Float64, Float64} = (NaN, NaN),
        ηp::Float64 = 0.7,
        ηm::Float64 = 0.9,
    )
    @assert Tₛmin < Tₛ′max
    @assert length(Qₗ) == length(c_el) == length(Tₐ) == length(c_ed)

    ts      = 1:length(Qₗ)
    unit    = 1:length(allunits)

    "Investment annuity factor"
    kₐ = Utils.annuityfactor(invrate, invtime)
    "Annual cost conversion factor to model period length (if model ≠ 8760 h)"
    kₚ = length(ts) * Δτ / 8760

    m = Model(solver = longterm ? mip_solver : nlp_solver)

    if !longterm
        @variable(m, Tₛ[0:length(ts)]  <= Tₛ′max) # DH supply temperature, mean
        @variable(m, Tₛ′[0:length(ts)] <= Tₛ′max) # DH supply temperature at plont
        @variable(m, Tᵣ[0:length(ts)]  <= Tret, start = Tret - 2.) # DH return temperature, mean
        @variable(m, Tᵣ′[0:length(ts)] == Tret) # DH return temperature at consumer

        @constraint(m, Tₛmin .<=  Tₛ[0:length(ts)])
        @constraint(m, Tₛ′min .<= Tₛ′[0:length(ts)])
        @constraint(m, 0. .<=  Tᵣ[0:length(ts)])
    end

    @variable(m, Qₛ[ts, unit] >= 0, start = 0.)  # Heat supply from unit
    @variable(m, Qᵣ[ts, unit] >= 0, start = 0.)  # Rejected heat
    @variable(m, ϕ[ts, unit] >= 0,  start = 0.)  # Fuel input (MW)
    @variable(m, Pᵢ[ts, unit] >= 0, start = 0.)  # Electricity input (MW)
    @variable(m, Pₒ[ts, unit] >= 0, start = 0.)  # Electricity output (MW)
    if longterm
        @variable(m, Qₓ[ts] >= 0, start = 4.)   # Heat loss from DH pipelines (MW)
        @variable(m, z[0:length(ts), unit], Bin, start = 0 )  # Unit commitment binary variable
        @variable(m, z₊[ts, unit],          Bin, start = 0 )  # Start-up
        @variable(m, z₋[ts, unit],          Bin, start = 0 )  # ShuTsown
    else
        # Initialize z: for all units with Lmin or SC, use presolved constraints with given values in function call. If longterm, constrain them later
        z = ones(Int64, length(ts), length(unit))
        if !isempty(fix_z)
            if strict_z
                z = fix_z
            else
                for u in unit
                    if (allunits[u].SC != 0.) || !isnan(allunits[u].Lmin)
                        z[:,u] = fix_z[:,u]
                    end
                end
            end
        end
        z₊ = zeros(Int64, length(ts), length(unit))
    end
    zₑ = ones(Int64, length(unit))

    # Costs
    @variable(m, C_o[ts, unit],      start = 0.)  # Operation and maintenance cost
    @variable(m, C_of[unit] >= 0,    start = 0.)  # Fixed operation and maintenance cost
    @variable(m, C_f[ts, unit] >= 0, start = 0.)  # Fuel cost
    @variable(m, C_e[ts, unit],      start = 0.)  # Electricity cost/profit
    @variable(m, C_s[ts, unit] >= 0, start = 0.)  # Start-up cost
    @variable(m, C_p[ts],            start = 0.)  # Cost of district heat pumping /€
    @variable(m, C_i[unit] >= 0,     start = 0.)  # Investment cost
    @variable(m, Cᵥ[ts, unit],       start = 0.)  # Total variable cost
    @variable(m, Cₓ[unit] >= 0,      start = 0.)  # Total fixed cost

    @variable(m, Pp[ts] >= 0, start = 0.)  # Power consumption in pumping (MW)

    @variable(m, Tslack[ts],  start = 0.)
    @variable(m, Tslack₊[ts], start = 0.)

    if longterm
        for t in ts
            JuMP.fix(Pp[t],  0.)
            JuMP.fix(C_p[t], 0.)
            JuMP.fix(Tslack[t], 0.)
            JuMP.fix(Tslack₊[t], 0.)
        end
    end

    for u in unit
        _ze_invest = get(fix_Qmax, u, 0.) == 0. ? 0 : 1
        _ze = allunits[u].Cᵢ == 0. ? 1 : _ze_invest
        zₑ[u] = _ze
    end

    ## DH network constraints

    # Approximation of mass flow rate
    # MFR: constant return temperature, supply temperature from adjustment curve
    ṁ0 = Qₗ ./ (cₚ * (supplytemperature.(Tₐ) - Tret)) #kg/s
    τₙ = (0.5 * ρ * V_dhn) ./  ṁ0  / 3600    # Network delay, h


    # Store inputs as fixed variable, which are not used in optimization
    @variable(m, heatload[ts]  == 0.)
    @variable(m, temp[ts] == 0.)
    @variable(m, elcost[ts] == 0.)
    for t in ts
        JuMP.fix(heatload[t], Qₗ[t])
        JuMP.fix(temp[t], Tₐ[t])
        JuMP.fix(elcost[t], c_el[t])
    end

    if !longterm
        # NLP model in the sliding window algorithm
        @variable(m, ṁ[ts] >= 0.)
        for t in ts # Set initial values
            setvalue(ṁ[t], ṁ0[t])
        end

        # Constraints at index t=0
        if isnan(Tₛ′0)
            @constraint(m, Tₛ′[1] == supplytemperature(Tₐ[1]))
            @constraint(m, Tₛ′[0] == Tₛ′[1])
        else
            @constraint(m, Tₛ′[0] == Tₛ′0)
        end
        isnan(Tₛ0)  ? @constraint(m, Tₛ[0]  == Tₛ[1])  : @constraint(m, Tₛ[0]  == Tₛ0)
        isnan(Tᵣ0)  ? @constraint(m, Tᵣ[0]  == Tᵣ[1])  : @constraint(m, Tᵣ[0]  == Tᵣ0)

        # Set start values for optimization, not constraints
        for t in ts
            setvalue(Tₛ[t],  supplytemperature(Tₐ[t]) - 5.)
            setvalue(Tₛ′[t], supplytemperature(Tₐ[t]))
        end
        setvalue(Tₛ[0],  isnan(Tₛ0)  ? supplytemperature(Tₐ[1]) - 5. : Tₛ0)
        setvalue(Tₛ′[0], isnan(Tₛ′0) ? supplytemperature(Tₐ[1])      : Tₛ′0)
    end

    # Network accumulation: handle whole DH network as one buffer

    if longterm
        # MILP model in the sliding window algorithm

        @constraint(m, [t in ts], sum(Qₛ[t, u] for u in unit) - Qₗ[t] - Qₓ[t]  == 0)

        # Heat loss. Estimates of supply and return temperatures: mean of previous 24h.
        _Tₛ = isnan(Tsm) ? supplytemperature(Tₐ[1]) : Tsm
        _Tᵣ = isnan(Trm) ? Tret : Trm
        @constraint(m, [t in ts],
            Qₓ[t] == cₚ * (0.5 * ρ * V_dhn) * kₓ * ((_Tₛ - Tₐ[t]) + (_Tᵣ - Tₐ[t]))
                / (3600 * Δτ))
    else
        # Reduction of DH network as one buffer \cite[Eq.~7]{Lesko2017}
        @NLconstraint(m, [t in ts],
            Tₛ[t] == Tₛ[t-1] + ṁ[t] * Δτ*3600 / (0.5 * ρ * V_dhn) * (Tₛ′[t] - Tₛ[t-1])
            - kₓ * (Tₛ[t] - Tₐ[t]) * Δτ
        )
        @NLconstraint(m, [t in ts],
            Tᵣ[t] == Tᵣ[t-1] + ṁ[t] * Δτ*3600 / (0.5 * ρ * V_dhn) * (Tᵣ′[t] - Tᵣ[t-1])
            - kₓ * (Tᵣ[t] - Tₐ[t]) * Δτ
        )

        # Heat supply
        @NLconstraint(m, [t in ts],
            sum(Qₛ[t, u] for u in unit) == cₚ * ṁ[t] * (Tₛ′[t] - Tᵣ[t] ))
        # Heat load
        @NLconstraint(m, [t in ts], Qₗ[t] == cₚ * ṁ[t] * (Tₛ[t]  - Tᵣ′[t]))

        # Hourly temperature change in the network
        @constraint(m, [t in ts], -ΔTₛ′max * Δτ <= (Tₛ′[t] - Tₛ′[t-1]))
        @constraint(m, [t in ts], (Tₛ′[t] - Tₛ′[t-1]) <= ΔTₛ′max * Δτ)

        @constraint(m, Tₛ′[ts] .== supplytemperature.(Tₐ[ts]) .+ Tslack[ts])
        @constraint(m, Tslack₊[ts] .>=  Tslack[ts])
        @constraint(m, Tslack₊[ts] .>= -Tslack[ts])
    end

    ## Electricity generation
    for u in unit
        if allunits[u].kind == "CHP"
            ηₑmax = allunits[u].Pmax / ((allunits[u].Qₛmax + allunits[u].Pmax) / allunits[u].ηₜ)
            @constraint(m, Pₒ[ts, u] .<= ϕ[ts, u] * ηₑmax)
        else
            @constraint(m, Pₒ[ts, u] .== 0) # No electricity generation
        end
    end

    ## Unit constraints

    # Get maximum heat supply from input data
    Qmax = [isnan(allunits[u].Qₛmax) ? get(fix_Qmax, u, 0.) : allunits[u].Qₛmax
            for u in unit]

    # Unit commitment / maximum power output
    for u in unit
        #NOTE if longterm, z is a variable, else array
        @constraint(m, Qₛ[ts, u] + Qᵣ[ts, u] .<= Qmax[u] * z[ts, u])
    end

    if longterm
        # Start-ups
        #   z[t, u] - z[t-1, u] generates -1 for shutdown, +1 for start-up
        @constraint(m, [t in ts, u in unit],
            z₊[t, u] - z₋[t, u] == z[t, u] - z[t-1, u] )

        # Only necessary units are on
        @constraint(m, z[ts, unit] .<= Qₛ[ts, unit] + Qᵣ[ts, unit])

        # If unit is on at any period, it exists
        @constraint(m, [t in ts, u in unit], zₑ[u] >= z[t, u])
    else
        for t in ts, u in unit
            if t == 1
                z₊[t, u] = max(0, z[t, u] - (isempty(z0) ? 1 : z0[u]) )
            else
                z₊[t, u] = max(0, z[t, u] - z[t-1, u])
            end
        end
    end

    # Set starting values for z at index 0
    if longterm && !isempty(z0)
        @constraint(m, [u in unit], z[0, u] == z0[u])
    end
    if longterm && !isempty(fix_z)
        @constraint(m, z[ts, unit] .== fix_z[ts, unit])
    end



    # NOTE if longterm, z is variable. Otherwise it is array.
    # Minimum power output
    for u in unit
        if allunits[u].kind == "CHP"
            # Minimum power output
            @constraint(m, Qₛ[ts, u] + Pₒ[ts, u] + Qᵣ[ts, u]
                .>= allunits[u].Lmin * (Qmax[u] + allunits[u].Pmax) * z[ts, u])
        elseif !isnan(allunits[u].Lmin)
            error("Lmin encountered but not yet implemented")
        end
    end
    # Allow heat rejection (=condensing mode) for CHP
    for u in unit
        if allunits[u].kind == "CHP"
            @constraint(m, Qᵣ[ts, u] .>= 0)
        else
            @constraint(m, Qᵣ[ts, u] .== 0)
        end
    end

    # Energy balance / fuel input
    for u in unit
        if allunits[u].kind == "HP"
            @constraint(m, Pᵢ[ts, u] * allunits[u].COP .== Qₛ[ts, u])
            @constraint(m, ϕ[ts, u] .== 0)
        elseif allunits[u].kind == "EB"
            @constraint(m, Pᵢ[ts, u] * allunits[u].ηₜ .== Qₛ[ts, u])
            @constraint(m, ϕ[ts, u] .== 0)
        else
            @constraint(m, ϕ[ts, u] * allunits[u].ηₜ .== Qₛ[ts, u] + Pₒ[ts, u] + Qᵣ[ts, u])
            @constraint(m, Pᵢ[ts, u] .== 0)
        end
    end

    # Fuel cost
    for u in unit
        _f = allunits[u].fuel
        !haskey(c_fuel, _f) && warn("Key $(_f) not in c_fuel")
        !haskey(c_fueltax,  _f) && warn("Key $(_f) not in c_fueltax")
        @constraint(m,
            C_f[ts, u] .==
            ϕ[ts, u] * (get(c_fuel, _f, 0) + get(c_fueltax, _f, 0)) * Δτ
            # €       =
            # MW fuel* (€/(MWh fuel)            + €/(MWh fuel)          ) * h
        )
    end

    if !longterm
        # Pumping cost
        if any(isnan.(oppoint))
            @constraint(m, Pp[ts] .== 0.)
            warn("Operation point oppoint is not defined in function call!")
        else
            mfr_o, Δp_o = oppoint
            @NLconstraint(m, [t in ts],
                Pp[t] == ṁ[t] ^3 * Δp_o / (mfr_o^2 * ρ * ηp * ηm) * 1e-6 # MW
            )
        end
        @constraint(m, [t in ts],
            C_p[t] == Δτ * Pp[t] * (c_el[t] + c_ed[t] + c_et)
        )
    end

    # Electricity cost/income
    for u in unit
        @constraint(m, [t in ts], C_e[t, u]
            == Δτ * Pᵢ[t, u] * (c_el[t] + c_ed[t] + c_et)
            - Δτ * Pₒ[t, u] * c_el[t]     # Income: profit (negative number)
        )
    end
    # FIXME multiply e_ecp by variable: in one month, largest hourly power consumption in mo-fri period
    # \cite{hsv2018siirto}

    # Operation and maintenance costs
    for u in unit
        if allunits[u].kind in ["EB", "HP"]
            Pimax = allunits[u].kind == "EB" ? Qmax[u] : Qmax[u] / allunits[u].COP
            @constraint(m, C_of[u] ==  kₚ * (
                Qmax[u] * allunits[u].Cof + Pimax * c_ecp + c_ecb * zₑ[u]))
                # € = MW DH * €/(MW DH)/a + MW el * €/MW el/a + €/a
        else
            @constraint(m, C_of[u] == kₚ * ( # kp: adjust to tot.hours/8760 (if partial year)
                Qmax[u] * allunits[u].Cof))
        end
        @constraint(m, C_o[ts, u] .== Qₛ[ts, u] * Δτ * allunits[u].Com)
        #              €           =  MW DH      * h  * €/(MWh DH)
    end

    # Start-up cost
    # NOTE if longterm, z₊ is variable, else array.
    @constraint(m, [t in ts, u in unit], C_s[t, u] == allunits[u].SC * z₊[t, u])

    # NOTE Shutdown costs not included

    # Annualized investment cost
    for u in unit
        if !isnan(allunits[u].Cᵢ)   # Should not happen in reference case
            @constraint(m, C_i[u] == allunits[u].Cᵢ * kₐ * kₚ * zₑ[u])
        else
            @constraint(m, C_i[u] == allunits[u].Cᵢ′ * Qmax[u] * kₐ * kₚ * zₑ[u])
        end
    end

    # Total variable and fixed cost
    @constraint(m, Cᵥ[ts, unit] .== C_f[ts, unit] + C_e[ts, unit] + C_o[ts, unit]
                        + C_s[ts, unit])
    @constraint(m, Cₓ[unit] .== C_i[unit] + C_of[unit])

    if !longterm && baseline
        @objective(m, Min,
            sum(Cᵥ[ts, unit]) + sum(Cₓ[unit]) + sum(C_p[ts])
            + sum(Tslack₊[ts]) * 1e3 )
    else
        @objective(m, Min, sum(Cᵥ[ts, unit]) + sum(Cₓ[unit]) + sum(C_p[ts]))
    end

    return m
end
