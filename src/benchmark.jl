"""
    save_stats(hours, modeltime, soltime, probclass, objval, tslack; filename="runtime.txt")

Save model solution statistics to file
"""
function save_stats(hours, modeltime, soltime, probclass, objval, tslack; filename="runtime.txt")
    writeheader = ~isfile(filename) || (read(filename, String) == "")
    gitcomm = try
        convert(String, chomp(readstring(`git describe --dirty`)))
    catch
        ""
    end
    open(filename, "a+") do f
        if writeheader
            writecsv(f, ["hours" "modeltime" "soltime" "probclass" "objval" "tslack" "gitcomm"])
        end
        writecsv(f, [hours modeltime soltime probclass objval tslack gitcomm])
    end
end

"""
    benchmark_runtime(; datafile = datafile, filename = "runtime.txt", max_hours = 8760)

Test runtime of DH models with increasing amount of hours
"""
function benchmark_runtime(; datafile = _f, filename = "runtime.txt", max_hours = 8760, verbose=1, args...)
    prev = 0
    fibo = 1
    while fibo < max_hours
        tmp = prev
        prev = fibo
        fibo += tmp
        if fibo < 24
            continue
        elseif fibo > max_hours
            fibo = max_hours
        end
        hours = fibo
        verbose > 0 && println("Running ", hours, " hour model...")
        times, temperature, heatload, elcost = Utils.readdata(datafile; limit_rows = hours)
        for bl in [false, true]
            modeltime = @elapsed m = generateDHmodel(;
                baseline=bl, Tₐ = temperature, Qₗ = heatload, c_el = elcost, args...)
            soltime = @elapsed solve(m)
            objval = getobjectivevalue(m)
            probclass = bl ? "baseline" : "optim"
            tslack = sum(getvalue(m[:Tslack₊]))
            save_stats(hours, modeltime, soltime, probclass, objval, tslack; filename=filename)
        end
    end
end
