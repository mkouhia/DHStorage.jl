# HACK testing environment does not work with PlotlyJS.
isdir(Pkg.dir("PlotlyJS")) && using PlotlyJS
using Colors: coloralpha, Colorant
using TimeZones

# https://stackoverflow.com/a/44727682
unit_colors = [
    "#1f77b4",  # muted blue
    "#ff7f0e",  # safety orange
    "#2ca02c",  # cooked asparagus green
    "#d62728",  # brick red
    "#9467bd",  # muted purple
    "#8c564b",  # chestnut brown
    "#e377c2",  # raspberry yogurt pink
    "#7f7f7f",  # middle gray
    "#bcbd22",  # curry yellow-green
    "#17becf"   # blue-teal
]
isdir(Pkg.dir("PlotlyJS")) && (unit_colors = Cycler(unit_colors))

unit_names = Dict("JarvenpaaC" => "BioCHP", "Kaskitie" => "HFO-HOB")

function plotlyjs_unit_color(unit::Int64; alpha=1.0)
    col = parse(Colorant, unit_colors[unit])
    @sprintf("rgba(%.3f, %.3f, %.3f, %.3f)", col.r, col.g, col.b, alpha)
end

"""
    check_plot_x_length(m::Dict{Symbol,Array{Float64,2}}, xvalues)

Check that length of `xvalues` matches `m[:heatload]` length.
Return `1:length(xvalues)`.
"""
function check_plot_x_length(res::Dict{Symbol,Array{Float64,2}}, xvalues)
    lx = length(res[:heatload])
    lx == length(xvalues) || throw(ArgumentError("Length of xvalues must match res[:heatload]"))
    return 1:lx
end

"""
    plotheat(res::Dict{Symbol,Array{Float64,2}}; xvalues, units)

Plot heat generation over time. Argument `xvalues` provides x-axis values.
"""
function plotheat(res::Dict{Symbol,Array{Float64,2}};
        xvalues = 1:length(res[:heatload]),
        units::Array{DHStorage.Unit,1} = DHStorage.units,
        limits=1:length(res[:heatload]),
        xlabel=true)
    ts = check_plot_x_length(res, xvalues)
    if isa(xvalues, Array{TimeZones.ZonedDateTime,1})
        xvalues = map(s -> s.utc_datetime, xvalues)
    end
    Qs = res[:Qₛ][limits,:]
    Qs[isapprox.(Qs, 0, atol = tol)] = 0
    Qs_cum = zeros(Float64, size(Qs)[1])
    draw_order = sortperm(vec(sum(Qs, 1)), rev=true)
    traces = Vector{PlotlyBase.GenericTrace{Dict{Symbol,Any}}}()
    for u in draw_order
        all(isapprox.(Qs[:, u], 0.; atol=1e-6)) && continue
        _fill = sum(Qs_cum) == 0. ? "tozeroy" : "tonexty"
        Qs_cum += Qs[:, u]
        t = scatter(; x = collect(xvalues)[limits], y = Qs_cum,
            fill = _fill, mode = "none", name = units[u].name,
            line_color = "transparent",
            fillcolor = plotlyjs_unit_color(u, alpha=0.7),
        )
        push!(traces, t)
    end
    push!(traces,
        scatter(; x = collect(xvalues)[limits], y = res[:heatload][limits],
            name = "Load", mode = "lines", line_color = unit_colors[6])
    )
    layout = Layout(;
        yaxis = attr(title="Heat /MW"),
        xaxis_title = xlabel ? "Time" : "",
    )
    plot(traces, layout)
end

"""
    plotpq(res::Dict{Symbol,Array{Float64,2}}, plotunit::Int64; units=DHStorage.units)

Plot P vs Q diagram for `units[plotunit]`.  Useful for CHP plants.
"""
function plotpq(res::Dict{Symbol,Array{Float64,2}}, plotunit::Int64;
        units::Array{DHStorage.Unit,1} = DHStorage.units)
    trace = scatter(; x = res[:Qₛ][:, plotunit], y = res[:Pₒ][:, plotunit],
        mode = "markers", name = units[plotunit].name)
    layout = Layout(;
        xaxis = attr(title="Qs /MW"),
        yaxis = attr(title="Po /MW"))
    plot([trace], layout)
end

"""
    plott(m::JuMP.Model, xvalues)

Plot temperature variation in DH network over time. Argument `xvalues`
provides x-axis values.
"""
function plott(res::Dict{Symbol,Array{Float64,2}};
        xvalues = 1:length(res[:heatload]),
        units::Array{DHStorage.Unit,1} = DHStorage.units,
        limits=1:length(res[:heatload]),
        xlabel=true,
        include_ret=true)
    ts = check_plot_x_length(res, xvalues)
    if isa(xvalues, Array{TimeZones.ZonedDateTime,1})
        xvalues = map(s -> s.utc_datetime, xvalues)
    end
    traces = [
        scatter(; x = collect(xvalues)[limits], y = res[:Tₛ][limits],  mode = "lines", name = "Tₛ"),
        scatter(; x = collect(xvalues)[limits], y = res[:Tₛ′][limits], mode = "lines", name = "Tₛ′"),

    ]
    if include_ret
        push!(traces, scatter(; x = collect(xvalues)[limits], y = res[:Tᵣ][limits],  mode = "lines", name = "Tᵣ"))
        push!(traces, scatter(; x = collect(xvalues)[limits], y = res[:Tᵣ′][limits], mode = "lines", name = "Tᵣ′"))
    end
    push!(traces, scatter(;
            x = collect(xvalues)[limits],
            y = supplytemperature.(res[:temp][limits]),
            mode = "lines", name = "θ")
    )
    layout = Layout(;
        yaxis = attr(title="Temperature /°C"),
        xaxis_title = xlabel ? "Time" : "",
    )
    plot(traces, layout)
end


"""
    plotm(res::Dict{Symbol,Array{Float64,2}}; xvalues, units)

Plot DH mass flow rate over time. Argument `xvalues` provides x-axis values.
"""
function plotm(res::Dict{Symbol,Array{Float64,2}};
        xvalues = 1:length(res[:heatload]),
        units::Array{DHStorage.Unit,1} = DHStorage.units,
        limits=1:length(res[:heatload]),
        xlabel=true)
    ts = check_plot_x_length(res, xvalues)
    if isa(xvalues, Array{TimeZones.ZonedDateTime,1})
        xvalues = map(s -> s.utc_datetime, xvalues)
    end
    trace = scatter(; x = xvalues, y = res[:ṁ][limits], mode="lines")
    layout = Layout(;
        yaxis = attr(title="Mass flow rate /(kg/s)"),
        xaxis_title = xlabel ? "Time" : "",
    )
    plot([trace], layout)
end
