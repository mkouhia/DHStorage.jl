using JuMP
using TimeZones
using JLD2

using .Utils


"""
    solve_sliding_window(;window_width = 5 * 24, advance_window = 24, args...)

Solve large problem with 5 day sliding window method.

Model is generated with `generateDHmodel`, to which any additional arguments
are also directed.

In order for the method to work, investments must be fixed. Thus, an error
will be thrown if there is no `fix_Qmax` parameter, and that dictionary has
keys for all u if `DHStorage.units[u].Cᵢ != 0`

# Algorithm
- generate large, original problem
- generate smaller problems and solve them in sequence
    - solve problem for first `window_width` hours
    - of that solution, take first `advance_window` hours and store that solution
    - solve next problem at t0[i] = t0[i-1] + advance_window
    - repeat by advancing window until all parts have been solved
- combine solution by fixing original problem's values to be equal to smaller
  problems' corresponding solutions with `JuMP.fix`
- solve original problem after fixing these values
- return orginal, solved problem

# Arguments
- `verbose::Int64=1`:  How much information to display. 0 -> quiet,
  1 -> display function prints only, 2 -> also display solver output
- `window_width_lt::Int64=30*24`: Long term model window width

"""
function solve_sliding_window(t_start::DateTime, t_stop::DateTime;
        advance_window::Int64 = 24,
        window_width::Int64 = 5 * 24,
        window_width_lt::Int64 = 30 * 24,
        result_file = "solution.jld2",
        result_comb_file = "solution_comb.jld2",
        return_result_dict::Bool = false,
        verbose::Int64 = 1,
        combine_solutions=true,
        args...
    )
    argdict = validate_sw_args(;args...)
    # Generate large model just to store its hash
    verbose > 0 && print("Generating large DH model...")
    temperature, heatload, elcost =
        map(s -> get_global_value(t_start, Dates.Hour(t_stop - t_start).value + 1, s),
            [:temperature :heatload :elcost])
    eldist = DHStorage.distributioncost.(t_start:Dates.Hour(1):t_stop)
    m_big    = generateDHmodel(; Tₐ = temperature, Qₗ = heatload, c_el = elcost, c_ed = eldist, args..., longterm=false)
    hash_slidingsolver = hash([window_width_lt, window_width, advance_window, temperature, heatload, elcost, eldist, argdict], hash(m_big))

    # Check if whole solution exists
    if combine_solutions && isfile(result_comb_file)
        isres = jldopen(result_comb_file, "r") do file
            haskey(file, repr(hash_slidingsolver))
        end
        if isres
            verbose > 0 && print(" using found solution in '$(result_comb_file)'.")
            r = load_complete_solution(hash_slidingsolver; filename = result_comb_file)
            verbose > 0 && println(" done.")
            return return_result_dict ? (m_big, r) : m_big
        end
    end

    m_big_lt = generateDHmodel(; Tₐ = temperature, Qₗ = heatload, c_el = elcost, c_ed = eldist, args..., longterm=true)
    allvars     = Symbol.(collect(Set(varname.(Variable.(m_big, 1:m_big.numCols)))))
    hash_slidingsolver_lt = hash([window_width_lt, advance_window, temperature, heatload, elcost, eldist, argdict], hash(m_big_lt))
    verbose > 0 && println(" done.")
    t0 = t_start; t1 = t_start + Dates.Hour(advance_window - 1)
    model_i = 0         # Running model index
    i_start = 0         # Starting index of this model (number of hours)
    presolved_solutions = isfile(result_file) ? true : false
    while t1 <= t_stop
        model_i += 1
        i_start += (model_i == 1) ? 1 : advance_window
        verbose > 0 && print("  Sub-model $model_i:")
        # Check for existing solution
        if presolved_solutions
            presolved_solutions =
                check_partial_solution(hash_slidingsolver, model_i; filename = result_file) &&
                check_partial_solution(hash_slidingsolver_lt, model_i; filename = result_file)
            if presolved_solutions
                verbose > 0 && println(" using found solution in '$(result_file)'.")
                # Set times for next iteration
                t0 += Dates.Hour(advance_window); t1 += Dates.Hour(advance_window)
                continue
            end
        end
        if t0 > t_start
            startvals = Dict{Symbol,Any}()
        end
        # times, temperature, heatload, elcost = Utils.readdata(_f)
        verbose > 0 && print(" generating longterm model...")
        if isfile(result_file) &&
                check_partial_solution(hash_slidingsolver_lt, model_i; filename = result_file)
            verbose > 0 && print(" using found solution in '$(result_file)'")
        else
            temperature_lt =   predict_temp(t0, window_width_lt)
            heatload_lt    = predict_demand(t0, window_width_lt)
            elcost_lt      = predict_elcost(t0, window_width_lt)
            eldist_lt      = DHStorage.distributioncost.(
                                t0 .+ Dates.Hour.(0:window_width_lt-1))
            if t0 == t_start
                m = generateDHmodel(;
                    Tₐ = temperature_lt, Qₗ = heatload_lt, c_el = elcost_lt,
                    c_ed = eldist_lt,
                    args...,
                    longterm = true)
            else
                lz, = load_partial_values(
                    hash_slidingsolver_lt, model_i-1, [:z];
                    filename = result_file)
                startvals[:z0] = round.(Integer, lz[1+advance_window, :])
                _sv = startvals
                # set starting S from NL model temperature
                lTs, lTr = load_partial_values(
                    hash_slidingsolver, model_i-1, [:Tₛ, :Tᵣ];
                    filename = result_file)
                # Get mean supply temperature from past 24h
                _sv[:Tsm] = mean(lTs[2:1+advance_window])
                _sv[:Trm] = mean(lTr[2:1+advance_window])
                m = generateDHmodel(;
                    Tₐ = temperature_lt, Qₗ = heatload_lt, c_el = elcost_lt,
                    c_ed = eldist_lt,
                    args..., _sv...,
                    longterm = true)
            end
            if isfile(result_file) &&
                    check_partial_solution(hash(m); filename=result_file,
                        solvehash = hash_slidingsolver_lt, idx = model_i)
                verbose > 0 && print(" found a solution in '$(result_file)'")
            else
                verbose > 0 && print(" solving...")
                _TT = STDOUT; verbose < 2 && redirect_stdout() # Direct output elsewhere
                ms = solve(m)
                verbose < 2 && redirect_stdout(_TT)
                @assert ms == :Optimal
                # Store solution somewhere
                verbose > 0 && print(" saving...")
                # No need to save all variables, only z
                save_partial_solution(m, model_i, ms, hash_slidingsolver_lt, [:z];
                    filename=result_file)
            end
        end
        verbose > 0 && print(" generating NL model...")
        zfix, = load_partial_values(
            hash_slidingsolver_lt, model_i, [:z]; filename = result_file)
        zfix = round.(Integer, zfix[1:window_width, :])
        temperature =   predict_temp(t0, window_width)
        heatload    = predict_demand(t0, window_width)
        elcost      = predict_elcost(t0, window_width)
        eldist      = DHStorage.distributioncost.(
                        t0 .+ Dates.Hour.(0:window_width-1))
        if t0 == t_start
            m = generateDHmodel(;
                Tₐ = temperature, Qₗ = heatload, c_el = elcost,
                c_ed = eldist,
                args...,
                longterm = false, fix_z = zfix)
        else
            _sv = startvals
            lTs, lTsp, lTr = load_partial_values(
                hash_slidingsolver, model_i-1, [:Tₛ :Tₛ′ :Tᵣ];
                filename = result_file)
            _sv[:Tₛ0]  = lTs[1+advance_window]
            _sv[:Tₛ′0] = lTsp[1+advance_window]
            _sv[:Tᵣ0]  = lTr[1+advance_window]
            m = generateDHmodel(;
                Tₐ = temperature, Qₗ = heatload, c_el = elcost,
                c_ed = eldist,
                _sv..., args...,
                longterm = false, fix_z = zfix)
        end
        if isfile(result_file) &&
                check_partial_solution(hash(m); filename=result_file,
                    solvehash = hash_slidingsolver, idx = model_i)
            verbose > 0 && println(" found a solution in '$(result_file)'.")
            # Set times for next iteration
            t0 += Dates.Hour(advance_window); t1 += Dates.Hour(advance_window)
            continue
        end
        verbose > 0 && print(" solving...")
        _TT = STDOUT; verbose < 2 && redirect_stdout() # Direct output elsewhere
        ms = solve(m)
        verbose < 2 && redirect_stdout(_TT)
        @assert ms == :Optimal
        # Store solution somewhere
        verbose > 0 && print(" saving...")
        save_partial_solution(m, model_i, ms, hash_slidingsolver, allvars;
            filename=result_file)
        verbose > 0 && println(" done.")
        # Set times for next iteration
        t0 += Dates.Hour(advance_window); t1 += Dates.Hour(advance_window)
    end
    # Combine partial solution and solve
    verbose > 0 && print("Combining solution...")
    r = combine_partial_solution(m_big, hash_slidingsolver, advance_window, window_width;
        varnames = allvars, filename=result_file)
    if combine_solutions
        verbose > 0 && print(" saving...")
        save_complete_solution(hash_slidingsolver, r; filename=result_comb_file)
    end
    verbose > 0 && println(" done.")
    return return_result_dict ? (m_big, r) : m_big
end

"Validate arguments for `solve_sliding_window`

Return arguments appropriate for hashing (excluding solver)"
function validate_sw_args(;args...)
    # Validate that proper `fix_Qmax` argument is found
    ad = Dict(args)
    if !haskey(ad, :fix_Qmax)
        error("Parameter `fix_Qmax` not in arguments (see `generateDHmodel` documentation).")
    else
        qd = ad[:fix_Qmax]
        for u in eachindex(DHStorage.units)
            uo = DHStorage.units[u]
            if uo.Cᵢ != 0 && ~haskey(qd, u)
                error("$u not found in fix_Qmax! Check documentation for `solve_sliding_window`.")
            end
        end
    end
    delete!(ad, :mip_solver)
    delete!(ad, :nlp_solver)
    :allunits in keys(ad) && (ad[:allunits] = hashable_units(ad[:allunits]))
    return ad
end

"""
    hashable_units(units::Array{DHStorage.Unit,1})

Convert units array to regular array, for hashing purposes
"""
hashable_units(units::Array{DHStorage.Unit,1}) =
    vcat([reshape([getfield(u, _f) for _f in fieldnames(u)], 1, :) for u in units]...)

function solve_sliding_window(t_start::ZonedDateTime, t_stop::ZonedDateTime; args...)
    solve_sliding_window(t_start.utc_datetime, t_stop.utc_datetime; args...)
end

function varname(var::JuMP.Variable)
    n = getname(var)
    endhere = searchindex(n, "[")
    s = endhere == 0 ? length(n) : endhere-1
    return n[1:s]
end

"""
    save_partial_solution(model::JuMP.Model, idx::Int64, solvestat::Symbol,
        solvehash::UInt64, varnames::Array{Symbol}; filename="solution.jld2")

Save partial solutions from sliding window method to disk. Data is stored in
JLD2 file followingly:
- file has two keys, `SW` and `parts`, which both contain `repr(hash)`es of
  `JuMP.Model` instances.
- `SW` has sliding window hashes, and includes key:value pairs for internal
  model indexes for partial solutions, and their hashes.
- `parts` contains partial solution hashes, and includes key:value pairs
  for model variables and their solutions as arrays.

    JLDFile
     ├─� SW
     │  ├─� 0xd83813f3247d8ba8
     │  │  ├─� 1
     │  │  ├─� ...
     │  └─� 0xa47c149e98ed7351
     │     ├─� 1
     │     ├─� ...
     └─� parts
        ├─� 0xc68d015b6ecfdb8c
        │  ├─� variable1
        │  ├─� ...
        ├─� 0x467bafcfb61ac1b5
        │  ├─� variable1
        │  ├─� ...
        ├─� ...
"""
function save_partial_solution(model::JuMP.Model, idx::Int64, solvestat::Symbol,
        solvehash::UInt64, varnames::Array{Symbol};
        filename="solution.jld2")
    _sh = repr(solvehash)
    jldopen(filename, "a+") do file
        mods  = haskey(file, "SW")    ? file["SW"]    : JLD2.Group(file, "SW")
        parts = haskey(file, "parts") ? file["parts"] : JLD2.Group(file, "parts")
        solver = haskey(mods, _sh) ? mods[_sh] : JLD2.Group(mods, _sh)
        mh = hash(model)
        solver[repr(idx)] = mh
        sols = JLD2.Group(parts, repr(mh))
        for sym in varnames
            sols[convert(String, sym)] =
                reshape(getvalue(model[sym][:]), length.(model[sym].indexsets))
        end
    end
end

"""
    check_partial_solution(solvehash::UInt64, idx::Int64; filename="solution.jld2")

Check for existing partial solution from sliding window method, based on
SW solver hash and index.
"""
function check_partial_solution(solvehash::UInt64, idx::Int64; filename="solution.jld2")
    jldopen(filename, "r") do file
        return haskey(file, "SW") &&
            haskey(file["SW"], repr(solvehash)) &&
            haskey(file["SW"][repr(solvehash)], repr(idx))
    end
end

"""
    check_partial_solution(parthash::UInt64; filename="solution.jld2", solvehash::UInt64=0x00, idx::Int64=0)

Check for existing partial solution from sliding window method, based on part hash.
Fill in `idx` to `solvehash` if it does not exist
"""
function check_partial_solution(parthash::UInt64; filename="solution.jld2",
        solvehash::UInt64=0x00, idx::Int64=0)
    jldopen(filename, "a+") do file
        parts = haskey(file, "parts") ? file["parts"] : JLD2.Group(file, "parts")
        _sh = repr(solvehash)
        if haskey(parts, repr(parthash))
            mods   = haskey(file, "SW") ? file["SW"] : JLD2.Group(file, "SW")
            solver = haskey(mods, _sh)  ? mods[_sh]  : JLD2.Group(mods, _sh )
            if !haskey(solver, repr(idx)) && solvehash != 0x00 && idx != 0
                solver[repr(idx)] = parthash
            end
            return true
        else
            return false
        end
    end
end

function load_partial_values(solvehash::UInt64, idx::Int64, symlist::Array{Symbol};
        filename="solution.jld2")
    jldopen(filename, "r") do file
        mh = repr(file["SW"][repr(solvehash)][repr(idx)])
        return [file["parts"][mh][convert(String, sym)] for sym in symlist]
    end
end

"""
    combine_partial_solution(<arguments>; filename="solution.jld2")

Combine partial solutions into a large one

Fix `model` values with the solved values in place.
Also return dictionary of recorded values.

# Arguments
- `model::JuMP.Model`:          Large model which is to be updated
- `hash_slidingsolver::UInt64`
- `advance_window::Int64`:      Length of accepted solution window, hours
- `window_width::Int64`:        Length of optimized window, hours

- `varnames::Array{Symbol}`:    Variable names to combine
- `filename="solution.jld2"`:   File name where to search for partial solutions.
"""
function combine_partial_solution(model::JuMP.Model, hash_slidingsolver::UInt64,
        advance_window::Int64, window_width::Int64;
        varnames::Array{Symbol} = Symbol[],
        filename="solution.jld2")
    varnames = (length(varnames) == 0) ?
        Symbol.(collect(Set(varname.(Variable.(model, 1:model.numCols))))) : varnames
    h = repr(hash_slidingsolver)
    d = Dict{Symbol,Array{Float64,2}}()
    jldopen(filename, "r") do file
        if ~haskey(file, "SW") || ~haskey(file["SW"], h)
            error("Hash of the model not found in $(filename).")
        end
        inds = sort(keys(file["SW"][h]), by=parse)
        for sym in varnames
            s = convert(String, sym)
            hash1 = repr(file["SW"][h]["1"])
            arr1 = file["parts"][hash1][s]
            if size(arr1)[1] in [window_width, window_width + 1]
                rows = (size(arr1)[1] == window_width) ?
                    (1:advance_window) : (2:advance_window+1)
                # Store in dictionary
                d[sym] = vcat([file["parts"][ repr(file["SW"][h][i]) ][s][rows,:] for i in inds]...)
                # Fix ts-indexed values
                rows = collect(rows)
                for ind in inds, ri in eachindex(rows)
                    r = rows[ri]
                    i_total = ri + (parse(ind) - 1) * advance_window
                    _arr = file["parts"][ repr(file["SW"][h][ind]) ]
                    if length(size(arr1)) == 1
                        JuMP.fix(model[sym][i_total], _arr[s][r])
                    elseif length(size(arr1)) == 2
                        for u in 1:size(arr1)[2]
                            JuMP.fix(model[sym][i_total, u], _arr[s][r, u])
                        end
                    else
                        error("Did not take into account >2 indexes for $(sym)")
                    end
                end
                # # Fix zero-index values
                # if size(arr1)[1] == window_width+1
                #     if length(size(arr1)) == 1
                #         JuMP.fix(model[sym][0], file[h]["1"]["solution"][s][1])
                #     else
                #         for u in 1:size(arr1)[2]
                #             JuMP.fix(model[sym][0, u],
                #                 file[h]["1"]["solution"][s][1, u])
                #         end
                #     end
                # end
            elseif size(arr1)[1] == length(DHStorage.units) && length(size(arr1)) == 1
                d[sym] =  maximum(hcat(
                    [file["parts"][ repr(file["SW"][h][i]) ][s][:] for i in inds]...), 2)
                for r in 1:size(arr1)[1]
                    JuMP.fix(model[sym][r], d[sym][r])
                end
            else
                error("Did not take into account indexes for $(sym)")
            end
        end
    end
    return d
end

function save_complete_solution(hash_slidingsolver::UInt64, result_dic::Dict{Symbol,Array{Float64,2}};
        filename="solution_comb.jld2")
    mh = repr(hash_slidingsolver)
    jldopen(filename, "a+") do file
        sol = haskey(file, mh) ? file[mh] : JLD2.Group(file, mh)
        for sym in keys(result_dic)
            sol[convert(String, sym)] = result_dic[sym]
        end
    end
end

"""
    load_complete_solution(hash_slidingsolver::UInt64; filename="solution_comb.jld2")

NOTE this does not update model m
"""
function load_complete_solution(hash_slidingsolver::UInt64; filename="solution_comb.jld2")
    mh = repr(hash_slidingsolver)
    d = Dict{Symbol,Array{Float64,2}}()
    jldopen(filename, "r") do file
        for sym_str in keys(file[mh])
            sym = Symbol(sym_str)
            d[sym] = file[mh][sym_str]
        end
    end
    return d
end

"""
    jldappend!(file1::String, file2::String)

Append contents of `file2` to `file1` JLD2 file.

Arguments `file1` and `file2` are file names.

NOTE if key exists in `file1` and `file2`, original content in `file1`
will not be replaced."""
function jldappend!(file1::String, file2::String)
    jldopen(file1, "a") do f1
        sw1    = haskey(f1, "SW")    ? f1["SW"]    : JLD2.Group(f1, "SW")
        parts1 = haskey(f1, "parts") ? f1["parts"] : JLD2.Group(f1, "parts")
        jldopen(file2, "r") do f2
            sw2    = haskey(f2, "SW")    ? f2["SW"]    : JLD2.Group(f2, "SW")
            parts2 = haskey(f2, "parts") ? f2["parts"] : JLD2.Group(f2, "parts")
            for sk in keys(sw2)
                key1 = haskey(sw1, sk) ? sw1[sk] : JLD2.Group(sw1, sk)
                key2 = sw2[sk]
                for p in keys(key2)
                    # Append SW records to file1 if they are not there
                    if !(p in keys(key1))
                        key1[p] = key2[p]
                    end
                end
            end
            for p2 in keys(parts2)
                key1 = haskey(parts1, p2) ? parts1[p2] : JLD2.Group(parts1, p2)
                key2 = parts2[p2]
                for var in keys(key2)
                    # Append parts records to file1 if they are not there
                    if !(var in keys(key1))
                        key1[var] = key2[var]
                    end
                end
            end
        end
    end
    return file1
end


"""
    predict_temp(t_now, hours)

Predict temperature (°C) for the next `hours` hours.

First element of result array is the temperature for this hour.
NOTE: using deterministic values from `DHStorage.temperature`
"""
predict_temp(t_now::DateTime, hours::Int64) =
    get_global_value(t_now, hours, :temperature)

"""
    predict_demand(t_now, hours)

Predict heat load (MW) for the next `hours` hours.

First element of result array is the temperature for this hour.
NOTE: using deterministic values from `DHStorage.heatload`
"""
predict_demand(t_now::DateTime, hours::Int64) =
    get_global_value(t_now, hours, :heatload)

"""
    predict_elcost(t_now, hours)

Predict electricity cost (€/MWh) for the next `hours` hours.

First element of result array is the temperature for this hour.
NOTE: using deterministic values from `DHStorage.elcost`
"""
predict_elcost(t_now::DateTime, hours::Int64) =
    get_global_value(t_now, hours, :elcost)

function get_global_value(t_now::DateTime, hours::Int64, sym::Symbol)
    # NOTE Not a real prediction, using deterministic values
    i0 = findfirst(DHStorage.times_utc, t_now)
    i1 = i0 + hours - 1
    # FIXME this will crash when approaching near end of 8760 model (data ends)
    getfield(DHStorage, sym)[i0:i1]
end

get_global_value(t_now::ZonedDateTime, hours::Int64, sym::Symbol) =
    get_global_value(t_now.utc_datetime, hours, sym)


"""
    totalcost(r)

Total annual cost

`r` is result dictionary from `solve_sliding_window`
"""
totalcost(r) = sum(r[:Cᵥ]) + sum(r[:C_p]) + sum(r[:Cₓ])

"""
    dhcost(r)

Hourly district heat cost

`r` is result dictionary from `solve_sliding_window`
"""
dhcost(r) = vec((sum(r[:Cᵥ], 2) .+ r[:C_p] .+ (sum(r[:Cₓ]) / length(r[:Cᵥ]))) ./ sum(r[:Qₛ], 2))

"""
    dhcost_avg(r)

Average cost of sold district heat, EUR/MWh

`r` is result dictionary from `solve_sliding_window`
"""
dhcost_avg(r) = totalcost(r) / sum(r[:heatload])
