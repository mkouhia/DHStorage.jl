module DHStorage

export
    generateDHmodel,
    load_solution, save_solution,
    plotheat, plotpq, plott, plots, plotm

include("utils.jl")
include("dh.jl")
include("plots.jl")
include("benchmark.jl")
include("solve.jl")
include("run.jl")

using .Utils: load_solution, save_solution

end # module
