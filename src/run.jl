using TimeZones

isdir(Pkg.dir("CPLEX")) ? using CPLEX : using Cbc
if isdir(Pkg.dir("Notifier"))
    using Notifier
    notify_able = true
else
    notify_able = false
end

simulation_arguments = Dict(
  :advance_window   => 24,
  :window_width     => 48,
  :window_width_lt  => 10*24,
  :mip_solver => isdir(Pkg.dir("CPLEX")) ? CplexSolver() : CbcSolver(),
  :fix_Qmax   => Dict(5=>0., 6=>0.),
  :verbose    => 1,
  :oppoint    => (500., 9.385e5),
  :kₓ         => 1.515e-2, # Target same annual heat losses as in 2017
  :ηp         => 0.7,
  :ηm         => 0.9,
  :ΔTₛ′max    => 7.,
  :V_dhn      => 5437.,
  :Tₛmin      => 65.,
  :Tₛ′min     => 75.,
  :Tret       => 45.,
  :baseline   => false,
  :return_result_dict => true,
)

function solve_all(; verbose = 1, send_email = true)
    t0 = ZonedDateTime(DateTime("2017-01-01"), TimeZone("Europe/Helsinki"))
    t1 = ZonedDateTime(DateTime("2017-12-31T23"), TimeZone("Europe/Helsinki"))
    args = simulation_arguments
    # args[:combine_solutions] = false
    fqd = []
    for qmax_hp in [0., 1., 5., 10., 15., 20., 30., 40.]
        push!(fqd, Dict(5 => qmax_hp, 6 => 0.))
    end
    push!(fqd, Dict(5 => 0., 6 => 10.))

    cases = []
    for fq in fqd, bl in [true false]
        push!(cases, Dict(:baseline => bl, :fix_Qmax => fq))
    end
    for dtsmax in 1.0:2.0:15.0, bl in [true, false]
        push!(cases, Dict(:ΔTₛ′max => dtsmax, :baseline => bl))
    end

    # CHP plant size
    for chp_qfac in 0.6:0.1:1.4, bl in [true, false]
        units = copy(DHStorage.units)
        _q = units[1].Qₛmax
        u1 = units[1]
        units[1] = DHStorage.Unit([_f == :Qₛmax ? _q * chp_qfac : getfield(u1, _f) for _f in fieldnames(u1)]...)
        push!(cases, Dict(:allunits => units, :baseline => bl))
    end

    # CHP efficiency
    for chp_eff in linspace(1.1, 0.85, 6), bl in [true, false]
        units = copy(DHStorage.units)
        u1 = units[1]
        units[1] = DHStorage.Unit([_f == :ηₜ ? chp_eff : getfield(u1, _f) for _f in fieldnames(u1)]...)
        push!(cases, Dict(:allunits => units, :baseline => bl))
    end

    # CHP start-up cost
    for suc in vcat(0:250:1500, 2500., 3500.), bl in [true, false]
        units = copy(DHStorage.units)
        u1 = units[1]
        units[1] = DHStorage.Unit([_f == :SC ? suc : getfield(u1, _f) for _f in fieldnames(u1)]...)
        push!(cases, Dict(:allunits => units, :baseline => bl))
    end

    # Electricity cost sensitivity without & with HP investment
    for qmax_hp in [0., 15.]
        fq = Dict(5 => qmax_hp, 6 => 0.)

        # Electricity cost: scaling
        for ec_scale in 0.6:0.2:1.4, bl in [true, false]
            push!(cases, Dict(:ec_scale => ec_scale, :baseline => bl, :fix_Qmax => fq))
        end
        # Electricity cost: variance
        for ec_var in 0.6:0.2:1.4, bl in [true, false]
            push!(cases, Dict(:ec_var => ec_var, :baseline => bl, :fix_Qmax => fq))
        end
    end

    function writerow(vec; overwrite=false)
        open("elcost_sen.csv", overwrite ? "w" : "a") do f
            for (i, v) in enumerate(vec)
                i > 1 && write(f, ',')
                write(f, string(v))
            end
            write(f, '\n')
        end
    end
    writerow(["totalcost" "dhcost" "ec_scale" "ec_var" "baseline" "args"]; overwrite=true)


    eval(DHStorage, :(_elcost0 = elcost))
    result = []

    totaltime = 0.; prev_time = 0.; elapsedtime = "";
    for (i, case_args) in enumerate(cases)
        if verbose >= 1
            elapsedtime = Dates.canonicalize(Dates.CompoundPeriod(Dates.Second(round(Int64, totaltime))))
            time_est = ( totaltime / (i-1) * 0.7 + prev_time * 0.3) * (length(cases) - i + 1)
            time_left = i == 1 ? NaN :
                Dates.canonicalize(Dates.CompoundPeriod(Dates.Second(
                    round(Int64, time_est))))
            argstr = join([string(repr(k), " => ", repr(case_args[k])) for k in keys(case_args)], ", ")
            msg = """Solving now i=$i of total $(length(cases)) cases.
            Extra arguments for this simulation:
                $argstr
            Total time elapsed: $elapsedtime.
            Estimated time left: $time_left.
            """
            send_email && notify_able && email(msg, subject="Simulation results", To="mikko.kouhia@aalto.fi")
            notify_able && notify(msg)
            println(msg)
        end
        if haskey(case_args, :ec_scale) || haskey(case_args, :ec_var)
            # Set electricity price scenario
            haskey(case_args, :ec_scale) &&
                eval(DHStorage, :(elcost = _elcost0 * $(case_args[:ec_scale])))
            haskey(case_args, :ec_var) &&
                eval(DHStorage, :(elcost = (_elcost0 .- mean(_elcost0)) .* $(case_args[:ec_var]) .+ mean(_elcost0) ))
            ca = copy(case_args)
            delete!(ca, :ec_scale); delete!(ca, :ec_var)
            prev_time = @elapsed m, r = DHStorage.solve_sliding_window(t0, t1; args..., ca..., :verbose => verbose)
            writerow(Any[DHStorage.totalcost(r),
                DHStorage.dhcost_avg(r),
                get(case_args, :ec_scale, ""), get(case_args, :ec_var, ""),
                case_args[:baseline],
                string(case_args)])
            eval(DHStorage, :(elcost = _elcost0))
        else
            prev_time = @elapsed m, r = DHStorage.solve_sliding_window(t0, t1; args..., case_args..., :verbose => verbose)
        end
        totaltime += prev_time
        push!(result, r)
    end
    if verbose >= 1
        msg = """Calculations ready!
        Total time elapsed: $elapsedtime."""
        send_email && email(msg, subject="Simulation results", To="mikko.kouhia@aalto.fi")
        notify_able && notify(msg)
        println(msg)
    end

    return result
end
