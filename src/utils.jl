module Utils

using JuMP
using JLD2
using CSV, DataFrames
using TimeZones

import Base.hash

"""
    annuityfactor(r::Float64, n::Int64)

Calculate annuity factor of an investment

Convert single investment into annual payment factors, given interest rate `r`
and investment length `n`.

# Examples
```jldoctest
julia> annuityfactor(0.08, 20)
0.10185220882315059
```
"""
function annuityfactor(r::Float64, n::Int64)
    return (r * (1 + r) ^ n) / ((1 + r) ^ n - 1)
end


"""
    readdata(filename, dtformat=Dates.DateFormat("yyyy-mm-dd HH:MM:SSz"), limit_rows::Int64=-1)

Read input data from CSV file.

Return arrays of datetime, ambient temperature, heat load, and electricity cost
as presented in input CSV file. Datetimes are converted to `ZonedDateTime`
objects.

# Examples

```jldoctest
shell> head -n 4 data/processed/optimization_data_sample.csv
datetime,T_a,Q_d,C_e
2017-01-01 01:00:00+02:00,2.433,33.679,24.030
2017-01-01 02:00:00+02:00,2.567,37.099,24.030
2017-01-01 03:00:00+02:00,1.967,39.974,24.020

julia> s = DHStorage.Utils.readdata(DHStorage._f, limit_rows=3)
(TimeZones.ZonedDateTime[2017-01-01T01:00:00+02:00, 2017-01-01T02:00:00+02:00, 2017-01-01T03:00:00+02:00], [2.433, 2.567, 1.967], [33.679, 37.099, 39.974], [24.03, 24.03, 24.02])

julia> typeof(s)
Tuple{Array{TimeZones.ZonedDateTime,1},Array{Float64,1},Array{Float64,1},Array{Float64,1}}
```
"""
function readdata(filename::String;
        dtformat::Dates.DateFormat = Dates.DateFormat("yyyy-mm-dd HH:MM:SSz"),
        limit_rows::Int64 = -1,
    )
    df = CSV.read(filename)

    # Limit data length if requested
    steps = (limit_rows == -1) ? (1:nrow(df)) : (1:limit_rows)
    heatload    = convert(Array{Float64}, df[steps, :Q_d])
    elcost      = convert(Array{Float64}, df[steps, :C_e])
    temperature = convert(Array{Float64}, df[steps, :T_a])
    times       = map(s -> ZonedDateTime(s, dtformat), df[steps, :datetime])

    return times, temperature, heatload, elcost
end

"""
    hash(m::JuMP.Model)

Create hash of JuMP model: write LP problem to temp file, hash the contents.
"""
function hash(m::JuMP.Model)
    f = tempname()
    writeLP(m, f)
    h = hash(read(f, String))
    rm(f)
    return h
end

"""
    save_solution(m::JuMP.Model; filename="solution.jld2")

Append model solution vector to JLD2 file.

Group saved solutions under hash of current model.

Raise error if solution contains NaN values.  If this is the case, model
is probably not solved.
"""
function save_solution(m::JuMP.Model; filename="solution.jld2")
    sol = m.colVal
    if any(isnan, sol)
        error("solution vector contains NaN values")
    end
    jldopen(filename, "a+") do file
        group = JLD2.Group(file, repr(hash(m)))
        group["colVal"] = sol
    end
end

"""
    load_solution(m::JuMP.Model, filename)

Load model solution from JLD2 file

Check model hash, load those solutions into the model that match the model.
Raise an error if there is no such solution"""
function load_solution(m::JuMP.Model, filename)
    h = hash(m)
    jldopen(filename, "r") do file
        m.colVal = file[repr(h)]["colVal"]
    end
end

end #Utils
