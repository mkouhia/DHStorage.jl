#!/usr/bin/env python
# -*- coding: UTF-8 -*-


'''Prepare data for JuMP simulation

'''


import os
import sys
import datetime

# import lxml # implicitly required by Pandas when importing html table
import pandas as pd
import demandlib.bdew

# Workalendar does not work with iPython.
import pkg_resources
workalendar_OK = True
try:
    import workalendar.europe
except pkg_resources.DistributionNotFound:
    workalendar_OK = False


# Temperature observations around 2017 bordering Järvenpää-Tuusula region
fmi_weather_files = [
    'data/temperature/csv-17609b6c-4d65-4359-be7b-3204403a5537.csv',
    'data/temperature/csv-5f0173d8-c112-43d9-8933-2305b3e3ff35.csv',
    'data/temperature/csv-be20514e-1a63-4209-a5b6-1c8fcba0f08f.csv']

spot_price_files = [
    'data/elspot-prices_2016_hourly_eur.xls',
    'data/elspot-prices_2017_hourly_eur.xls',
    'data/elspot-prices_2018_hourly_eur.xls'
]

holidays_2017 = {
    datetime.date(2017, 1, 1): 'New year',
    datetime.date(2017, 1, 6): 'Epiphany',
    datetime.date(2017, 4, 14): 'Good Friday',
    datetime.date(2017, 4, 16): 'Easter Sunday',
    datetime.date(2017, 4, 17): 'Easter Monday',
    datetime.date(2017, 5, 1): 'Labour Day',
    datetime.date(2017, 5, 25): 'Ascension Thursday',
    datetime.date(2017, 6, 4): 'Pentecost',
    datetime.date(2017, 6, 23): "Midsummer's Eve",
    datetime.date(2017, 6, 24): "Midsummer's Day",
    datetime.date(2017, 11, 4): 'All Saints',
    datetime.date(2017, 12, 6): 'Independence Day',
    datetime.date(2017, 12, 24): 'Christmas Eve',
    datetime.date(2017, 12, 25): 'Christmas Day',
    datetime.date(2017, 12, 26): "St. Stephen's Day"}


def avg_temperature(*files):
    '''Get average temperature from multiple FMI files

    Return data frame with average temperature'''
    for i, file in enumerate(files):
        if i == 0:
            df = read_fmi(file)
        else:
            df = df.join(read_fmi(file), rsuffix = str(i))
    return pd.DataFrame(index = df.index,
                        data = {'temp.C' : df.mean(axis = 1,skipna = True)})

def read_fmi(filename):
    '''Read FMI weather data into pandas data frame'''
    d = pd.read_csv(filename, parse_dates=[['Vuosi','Kk','Pv', 'Klo']]).\
            rename(columns = {'Vuosi_Kk_Pv_Klo' : 'datetime'}).\
            drop(['Aikavyöhyke'], axis = 1).\
            set_index('datetime')
    d.index = d.index.tz_localize('UTC').tz_convert('Europe/Helsinki')
    return d


def bdew_heat_demand(*weather_files):
    '''Generate simulated consumption profile for a district heating network

    The defaults output simulated DH consumption for Järvenpää-Tuusula DH network.
    '''
    # Average temperatures
    d = avg_temperature(*fmi_weather_files)

    #d = d[d.index.year == year]

    years = set(d.index.year)
    # Holidays
    if workalendar_OK:
        cal = workalendar.europe.Finland()
        _hd = []
        for y in years:
            _hd.extend(cal.holidays(y))
        holidays = dict(_hd)
    else:
        assert len(years) == 1 and 2017 in years
        holidays = holidays_2017

    # Simulated demand
    # NOTE parameter annual_heat_demand is only for scaling normalized output,
    # and it will be the sum of heat consumption OVER THE WHOLE INDEX.
    dem_2017 = 332.9e6 # kWh [Energiateollisuus vuositaulukot 2017]
    building_types = ["MFH", "EFH", "GHD"]
    for typ in building_types:
        key = "demand" + typ
        d[key] = demandlib.bdew.HeatBuilding(
            d.index,
            holidays = holidays,
            temperature = d['temp.C'],
            shlp_type = typ,  # Mehrfamilienhaus = block of flats
            building_class = 1 if typ != "GHD" else 0, # Seems proper
            wind_class = 0,     # Not windy
            annual_heat_demand = 100e6, # kWh 
            ww_incl = True,     # Warm water included
            name = 'Aggregated buildings').get_bdew_profile() / 1000
        # Do proper scaling
        scaler = dem_2017 * 1e-3 / sum(d[d.index.year == 2017][key])
        d[key] = scaler * d[key]

    d["demand.MWh"] = 0.4 * d["demandMFH"] + 0.3 * d["demandEFH"] + 0.3 * d["demandGHD"]
    d.drop(columns = ["demandMFH", "demandEFH", "demandGHD"])

    return d


def read_spotprices(file, price_region=None):
    '''Read elspot prices from file

    File is in Excel HTML table, as downloaded from nordpoolspot.com

    The times are in CET time, so extra attention needs in data alignment.

    price_region is either str corresponding to Nordpool price region,
    or iterable of strings.
    '''
    df = pd.read_html(file, encoding = 'UTF-8', skiprows = 0, header = 2,
                      decimal = ',', thousands = None)[0]
    df.rename(columns = {df.columns[0]:'Date'}, inplace = True)

    #Add timestamp column
    df['datetime'] = pd.to_datetime(
        df.Date + 'T' + df.Hours.str.extract('(\d\d)\s-\s\d\d').iloc[:,0] + ':00')

    # Filter out non-existent time
    df['NaT'] = df.datetime.map(
        lambda t: type(t.tz_localize('CET', errors='coerce', ambiguous=True))
                is pd._libs.tslibs.nattype.NaTType)
    df = df[~df.NaT]

    # Set index
    df = df.set_index('datetime')
    df = df.set_index(df.index.tz_localize('CET', ambiguous='infer'))

    # Clean intermediate columns
    df = df.drop(['Date', 'Hours', 'NaT'], axis = 1)

    # Select columns and return
    if price_region is None:
        return df
    elif isinstance(price_region, str):
        return df[[price_region]]
    else:
        return df[[*price_region]]


if __name__ == '__main__':
    if not os.path.exists('data/processed'):
        os.mkdir('data/processed')
    print('Processing weather files')
    d1 = bdew_heat_demand(*fmi_weather_files)
    print('Processing electricity price files')
    d2 = f = pd.concat([read_spotprices(i, 'FI') for i in spot_price_files])
    print('Joining and writing data')
    d3 = d1.join(d2, how = 'inner')\
        .tz_convert("Europe/Helsinki")\
        .rename(columns = {'temp.C': 'T_a', 'demand.MWh': 'Q_d', 'FI': 'C_e',})
    print('  data/processed/optimization_data.csv')
    d3.to_csv('data/processed/optimization_data.csv', float_format = '%.3f')
    d3.head(48).to_csv('data/processed/optimization_data_sample.csv', float_format = '%.3f')
    metadata = '''# Processed data for JuMP optimization.

## Columns
- datetime: timezone-aware time stamp. Hourly resolution. Start of time period.
- T_a:      ambient temperature (degrees Celsius). Averaged temperature for
    Järvenpää-Tuusula region, derived from FMI data.
- Q_d:      Simulated average DH demand over the period (MWh/h).
- C_e:      Electricity cost (EUR/MWh), from Nord Pool Spot.
'''
    print('  data/processed/optimization_data.metadata.csv')
    with open('data/processed/optimization_data.metadata.csv', 'w') as f:
        f.write(metadata)
    print('Ready!')
