# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Investment cost for plants
- Option to invest in new plants, size either fixed or free
  - Add electric boilers
  - Add heat pumps
- Electricity tax and distribution cost
- Tolerance for some comparisons
- Utility function: annuity factor

### Changed
- Rename and reorder cost variables
- Divide electricity input and output to separate variables
- Use Plots backend for graphing instead of Gadfly
- Date axis in plots

### Fixed
- Constraints for rejected heat

## 0.1.0 - 2018-06-20
### Added
- First working version of DH generation optimization program

[Unreleased]: https://gitlab.com/mkouhia/DHStorage.jl/compare/0.1.0...HEAD
