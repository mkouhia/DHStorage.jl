# District heat network as a short-term energy storage

This optimization program examines the possibility and effects of district heat supply temperature optimization.
The program is introduced and the results discussed in [an article submitted to journal Energy][article] (preprint as of February 2019, link to be added upon possible acceptance).

The temperature in district heat network is usually controlled based on supply temperature curve, thus the feed temperature in summer can be around 70 degrees Celsius in the summer and above 110 degrees in the winter.
The delay in DH networks is in the order of hours: when heat is added to the network in the central plant, the pulse of water reaches the customers usually 0,5..3 hours afterwards.
These factors in common imply that there is potential in the district heating network to be employed as a short-term heat storage, which does not require any investment costs.
In this project we examine if this potential can be utilized sensibly.

The model may be employed as-is to reproduce results in the [journal paper][article], or modified to evaluate other district heat systems.
Only data on heat load, ambient temperatures and electricity cost are required, in addition to key parameters of the network.
Full flow model of the DH network is not required, as the model reduces the network into buffers.

Please see the [accompanying article][article] for explanation of the model and discussion of its applications.

If you want me to adapt the model to your specific needs, I am available for consulting services. [See contact details][contact].

**Note:** the application uses Julia version 0.6, which is now outdated. The package will not be developed further, unless agreed upon.

[article]: https://users.aalto.fi/~mkouhia/pub/2019-02-13_dhstorage.pdf
[contact]: https://users.aalto.fi/~mkouhia/about/


## License

The software is licensed under the terms of the MIT license, see [LICENSE.md](LICENSE.md).



## Installation

-   Copy package to local machine and run tests
    ```julia
    Pkg.clone("git@gitlab.com:mkouhia/DHStorage.jl.git")
    Pkg.test(DHStorage)
    ```

## Usage

### Reproducing article results

#### Generating input data (optional)

There is a Python script in place to process weather data and electricity price files.
The output optimization data is already included in this repository in [data/processed/optimization_data.csv](data/processed/optimization_data.csv), so you do not necessarily need to regenerate it.
Do following to create dataset for optimization.

- Install `pipenv`
  ```bash
  $ pip install --user pipenv
  ```
- Add pip user path to PATH if it is not there already
  ```bash
  $ export PATH=$PATH:"~/.local/bin"
  ```
- Install required packages
  ```bash
  $ pipenv install
  ```
- Activate python environment
  ```bash
  $ pipenv shell
  ```
- Run processing script
  ```bash
  (DHStorage.jl)$ python scripts/prepare_data.py 
  Processing weather files
  Processing electricity price files
  Joining and writing data
    data/processed/optimization_data.csv
    data/processed/optimization_data.metadata.csv
  Ready!
  ```

#### Optimization

-   Running optimization from Julia prompt
    ```julia
    using DHStorage, TimeZones
    t0 = ZonedDateTime(DateTime("2017-01-01"), TimeZone("Europe/Helsinki"))
    t1 = ZonedDateTime(DateTime("2017-12-31T23"), TimeZone("Europe/Helsinki"))
    
    # Outcome with supply temperature optimization
    m, result_opt = DHStorage.solve_sliding_window(t0, t1; DHStorage.simulation_arguments..., baseline=false)

    # Reference outcome
    m, result_ref = DHStorage.solve_sliding_window(t0, t1; DHStorage.simulation_arguments..., baseline=true)
    
    # Outcome with 20 MW heat pump
    m, result_opt = DHStorage.solve_sliding_window(t0, t1; DHStorage.simulation_arguments..., baseline=false, fix_Qmax=Dict(5 => 20., 6 => 0.))
    
    ```
-   Plot results
    ```julia
    DHStorage.plotheat(m)
    DHStorage.plott(m)
    ```
-   To generate all results in the article you may do following (**note:** this will take a lot of time!)
    ```julia
    results = DHStorage.solve_all()
    ```

### Description of programming logic

Function `solve_sliding_window` in `solve.jl` is in charge of the sliding window solution algorithm.
It will call `generateDHmodel` to form the MILP and NLP models, and solve them consecutively

### Adaption to other problems

- As of now, `solve_sliding_window` uses actual energy system data instead of predictions, with function `get_global_value` reading data from variables `times, temperature, heatload, elcost` in `dh.jl`.
  To implement forecasts, edit those functions. Alternatively, insert your data into `optimization_data.csv`.
- `solve_sliding_window` feeds arguments to `generateDHmodel()`, see its source code in `dh.jl` for all possible parameters.
  To adapt to other networks, you will need to change them; most importantly

    - `allunits::Array{Unit,1}`:             Units available in optimization
    - `c_fuel::Dict{String,Float64}`:       Fuel costs (EUR/MWh,LHV)
    - `c_fueltax::Dict{String,Float64}`:    Additional fuel tax (EUR/MWh,LHV)

  Refer to code in `dh.jl` to see how to input different plants.
  Arguments `Qₗ`,  `c_el` and `Tₐ` are handled by `solve_sliding_window`; fix those prediction functions to affect these.
- The supply temperature curve for the reference method is hard-coded.
  To edit the reference supply temperature curve, edit default parameters of `supplytemperature` in code to fix that.

## Contribution

Contributions are accepted, and welcomed.
If you want to contribute, please [contact me][contact].
