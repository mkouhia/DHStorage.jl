using JuMP
using Ipopt
import DHStorage.tol

times, temperature, heatload, elcost = DHStorage.Utils.readdata(DHStorage._f; limit_rows = 48)
m = DHStorage.generateDHmodel(; Tₐ = temperature, Qₗ = heatload, c_el = elcost, oppoint=(500., 7.985e5), longterm=false)
ts = 1:length(times)
units = DHStorage.units
unit = 1:length(units)

@testset "Input data" begin
    @test isfile(DHStorage.datafile) || isfile(DHStorage.datafile_fallback)
end

@testset "Model generation" begin
    @test_nowarn DHStorage.generateDHmodel(; oppoint=(500., 7.985e5))
    @test_nowarn DHStorage.generateDHmodel(; oppoint=(500., 7.985e5), baseline = true)
    @test_nowarn DHStorage.generateDHmodel(; longterm = true)
    @test_nowarn DHStorage.generateDHmodel(; longterm = true, baseline = true)
end

@testset "Optimal solution is found" begin
    @test solve(m) == :Optimal
end


@testset "Solutions" begin
    @testset "Temperatures do not cross" begin
        for t in ts
            @test getvalue(m[:Tᵣ][t])  < getvalue(m[:Tₛ][t])
            @test getvalue(m[:Tᵣ′][t]) < getvalue(m[:Tₛ][t])
            @test getvalue(m[:Tᵣ][t])  < getvalue(m[:Tₛ′][t])
            @test getvalue(m[:Tᵣ′][t]) < getvalue(m[:Tₛ′][t])
        end
    end

    @testset "Electricity generation" begin
        for t in ts, u in unit
            p = getvalue(m[:Pₒ][t, u])
            if units[u].kind == "CHP"
                @test p >= 0
            else
                @test isapprox(p , 0; atol=tol)
            end
        end
    end


    @testset "Energy balance" begin
        for t in ts, u in unit
            _ϕ = getvalue(m[:ϕ][t, u])
            _q = getvalue(m[:Qₛ][t, u])
            _r = getvalue(m[:Qᵣ][t, u])
            _pi = getvalue(m[:Pᵢ][t, u])
            _po = getvalue(m[:Pₒ][t, u])
            _η = units[u].ηₜ
            if _q ≈ 0
                @test isapprox(_ϕ , 0; atol=tol)
                @test isapprox(_r , 0; atol=tol)
                @test isapprox(_pi , 0; atol=tol)
                @test isapprox(_po , 0; atol=tol)
            elseif units[u].kind == "EB"
                @test isapprox(_pi * _η , _q; atol=tol)
                @test isapprox(_ϕ , 0; atol=tol)
            elseif units[u].kind == "HP"
                @test isapprox(_pi * units[u].COP , _q; atol=tol)
                @test isapprox(_ϕ , 0; atol=tol)
            else
                @test isapprox((_ϕ + _pi) * _η , _q + _po + _r; atol=tol)
            end
        end
    end
    @testset "Rejected heat" begin
        for t in ts, u in unit
            r = getvalue(m[:Qᵣ][t, u])
            if units[u].kind == "CHP"
                @test r >= 0
            else
                @test isapprox(r , 0; atol=tol)
            end
        end
    end

    @testset "Fuel costs" begin
        for t in ts, u in unit
            if getvalue(m[:Qₛ][t, u]) == 0
                @test isapprox(getvalue(m[:C_f][t, u]) , 0; atol=tol)
            elseif units[u].kind in ["EB" "HP"]
                @test isapprox(getvalue(m[:C_f][t, u]) , 0; atol=tol)
            else
                @test getvalue(m[:C_f][t, u]) >= 0
            end
        end
    end
    @testset "Electricity costs" begin
        _tol = 1e-6 # NOTE Increase tolerance
        for t in ts, u in unit
            _ce = getvalue(m[:C_e][t, u])
            if getvalue(m[:Qₛ][t, u]) == 0
                @test isapprox(_ce, 0.; atol=_tol)
            elseif units[u].kind == "CHP"
                @test _ce < 0. || isapprox(_ce, 0.; atol=_tol)
            elseif units[u].kind in ["EB" "HP"]
                @test _ce > 0. || isapprox(_ce, 0.; atol=_tol)
            else
                @test isapprox(_ce, 0.; atol=_tol)
            end
        end
    end
end
