using JuMP, JLD2

function testjld(filename1, filename2, has_all=true)
    jldopen(filename1, "r") do _f1
        jldopen(filename2, "r") do _f2
            @testset for k in ["SW", "parts"]
                if has_all
                    @test Set(keys(_f2[k])) == Set(keys(_f1[k]))
                else
                    @test issubset(Set(keys(_f2[k])), Set(keys(_f1[k])))
                    @test !issubset(Set(keys(_f1[k])), Set(keys(_f2[k])))
                end
                @testset for phash in keys(_f2[k])
                    @test Set(keys(_f2[k][phash])) == Set(keys(_f1[k][phash]))
                    @testset for k2 in keys(_f2[k][phash])
                        @test _f2[k][phash][k2] == _f1[k][phash][k2]
                    end
                end
            end
        end
    end
end

@testset "Sliding window model" begin
    t0 = DHStorage.times[1]
    t1 = DHStorage.times[24]
    fn = tempname(); fnc = tempname()
    args = Dict(:advance_window => 6, :window_width => 18, :window_width_lt => 24,
            :result_file => fn,
            :result_comb_file => fnc,
            :combine_solutions => false,
            :verbose => 0,
            :oppoint => (500., 7.985e5),
            :V_dhn      => 5356.,
            :Tₛmin      => 50.,
    )
    rm(fn, force=true); rm(fnc, force=true) #HACK jldopen(tempname(), "a+") throws EOFerror`
    @testset "Qmax parameter" begin
        @testset "Expected errors in validation" begin
            @test_throws Base.ErrorException DHStorage.solve_sliding_window(
                t0, t1; args...)
            @test_throws Base.ErrorException DHStorage.solve_sliding_window(
                t0, t1; fix_Qmax = Dict(5=>0.), args...)
            @test_throws Base.ErrorException DHStorage.solve_sliding_window(
                t0, t1; fix_Qmax = Dict(6=>0.), args...)
        end
        @testset "Valid parameter values" begin
            @test_nowarn DHStorage.solve_sliding_window(
                t0, t1; fix_Qmax = Dict(5=>0., 6=>0.), args...)
            @test_nowarn DHStorage.solve_sliding_window(
                t0, t1; fix_Qmax = Dict(5=>1., 6=>1.), args...)
        end
    end
    @testset "Baseline" begin
        @test_nowarn DHStorage.solve_sliding_window(t0, t1;
            fix_Qmax = Dict(5=>0., 6=>0.), args...)
        @test_nowarn DHStorage.solve_sliding_window(t0, t1;
            fix_Qmax = Dict(5=>0., 6=>0.), baseline=true, args...)
    end
    @testset "Check saved solutions" begin
        _args = Dict(
            :advance_window => 4, :window_width => 12, :window_width_lt => 24,
            :fix_Qmax => Dict(5=>0., 6=>0.),
        )
        _t0 = DHStorage.times[1].utc_datetime
        _t1 = DHStorage.times[24].utc_datetime
        _temp, _hl, _ec = map(
            s -> DHStorage.get_global_value(
                _t0, Dates.Hour(_t1 - _t0).value + 1, s),
            [:temperature :heatload :elcost])
        _ed = DHStorage.distributioncost.(_t0:Dates.Hour(1):_t1)
        _m = DHStorage.generateDHmodel(;
            Tₐ = _temp, Qₗ = _hl, c_el = _ec, c_ed = _ed, fix_Qmax=Dict(5=>0., 6=>0.),
            oppoint=(500., 7.985e5), Tₛmin = 50.)
        _h = hash([24, 12, 4, _temp, _hl, _ec, _ed, Dict(
                :fix_Qmax => Dict(5=>0., 6=>0.),
                :oppoint => (500., 7.985e5),
                :V_dhn      => 5356.,
                :Tₛmin      => 50.,)],
            hash(_m))
        @test DHStorage.check_partial_solution(_h, 1, filename=fn) == false
        @test_nowarn DHStorage.solve_sliding_window(
            _t0, _t1; args..., _args...)
        @testset "Partial solution: $i found" for i in 1:6
            @test DHStorage.check_partial_solution(_h, i; filename=fn)
        end
        @test DHStorage.check_partial_solution(_h, 7; filename=fn) == false
        @testset "Using partial solutions in another problem" begin
            @test_nowarn DHStorage.solve_sliding_window(
                _t0, DHStorage.times[20].utc_datetime; args..., _args...)
        end

        @testset "Appending of JLD2 file" begin
            fn2 = tempname(); rm(fn2, force=true);
            @test_nowarn DHStorage.solve_sliding_window(
                _t0, DHStorage.times[20].utc_datetime; args..., _args...,
                result_file=fn2)
            testjld(fn, fn2, false)
            @test_nowarn DHStorage.jldappend!(fn2, fn)
            testjld(fn, fn2, true)
            rm(fn2, force=true)
        end
    end
    @testset "Prediction at end of dataset" begin
        t1 = DHStorage.times[end]
        t0 = t1 - Dates.Hour(48)
        @test_broken DHStorage.solve_sliding_window(t0, t1;
            fix_Qmax = Dict(5=>0., 6=>0.), args...)
    end
    @testset "Results from getvalue and returned array match" begin
        m, r = @test_nowarn DHStorage.solve_sliding_window(
            DHStorage.times[1], DHStorage.times[24];
            fix_Qmax = Dict(5=>0., 6=>0.), return_result_dict=true,
            args...)
        allvars = Symbol.(collect(Set(DHStorage.varname.(Variable.(m, 1:m.numCols)))))
        @testset "Var: $sym" for sym in allvars
            isets = m[sym].indexsets
            for i in isets[1]
                i == 0 && continue
                if length(isets) == 1
                    @test getvalue(m[sym][i]) == r[sym][i]
                else
                    for j in isets[2]
                        @test getvalue(m[sym][i,j]) == r[sym][i,j]
                    end
                end
            end
        end
    end
    rm(fn, force=true) # Remove test file
end

@testset "Predictor functions" begin
    t0 = DateTime(2016, 12, 31, 23)
    @testset "Length OK" begin
        @test length(DHStorage.predict_temp(t0, 24)) == 24
        @test length(DHStorage.predict_demand(t0, 24)) == 24
        @test length(DHStorage.predict_elcost(t0, 24)) == 24
        @test length(DHStorage.get_global_value(t0, 24, :temperature)) == 24
    end
    @testset "Values" begin
        @test all(DHStorage.predict_temp(t0, 3)   .== [ 2.433,  2.567,  1.967])
        @test all(DHStorage.predict_elcost(t0, 3) .== [24.03,  24.03,  24.02 ])
    end
end
