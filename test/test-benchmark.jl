@testset "Saving of runtime statistics" begin
    f = tempname()
    @test_nowarn DHStorage.save_stats(48, 0.7, 3.1, "baseline", 55000.1, 0.0; filename = f)
    @test_nowarn DHStorage.save_stats(48, 0.9, 3.3, "optim", 50000.1, 10.0; filename = f)
    l = @test_nowarn readlines(f)
    @test l[1] == "hours,modeltime,soltime,probclass,objval,tslack,gitcomm"
    @test startswith(l[2], "48,0.7,3.1,baseline,55000.1,0,")
    @test startswith(l[3], "48,0.9,3.3,optim,50000.1,10,")
    @test length(split(l[1], ",")) == length(split(l[2], ","))
end

@testset "Running over small test set: no errors" begin
    f = tempname()
    @test_nowarn DHStorage.benchmark_runtime(; filename = f, max_hours = 48, oppoint=(500., 7.985e5), verbose=0)
    @test isfile(f) && length(read(f)) > 0
end
