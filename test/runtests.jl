using DHStorage
using Base.Test

@testset "All tests" begin
    for testname in ["utils" "dh" "benchmark" "solve"]
        @testset "$testname" begin
            include("test-$(testname).jl")
        end
    end
end
