using JuMP, Cbc
using JLD2


function sample_model()
    m = Model(solver=CbcSolver())
    @variable(m, 0 <= x <= 5)
    @variable(m, 0 <= y <= 10)
    @objective(m, Max, 4x + 2y)
    @constraint(m, 2x + 5y <= 3.0)
    return m
end


@testset "Annuity factor" begin
    @test_throws MethodError DHStorage.Utils.annuityfactor(0.1, 10.1)
    @test DHStorage.Utils.annuityfactor(0.1, 10) ≈ 0.16274539488251152
end

@testset "Supply temperature" begin
    @test DHStorage.supplytemperature( 10.0; Ta0=5.0, Ta1=-30.0, Ts0=75.0, Ts1=120.0) ==  75.0
    @test DHStorage.supplytemperature(  5.0; Ta0=5.0, Ta1=-30.0, Ts0=75.0, Ts1=120.0) ==  75.0
    @test DHStorage.supplytemperature(  0.0; Ta0=5.0, Ta1=-30.0, Ts0=75.0, Ts1=120.0)  ≈  81.42857142857143
    @test DHStorage.supplytemperature(-15.0; Ta0=5.0, Ta1=-30.0, Ts0=75.0, Ts1=120.0)  ≈ 100.71428571428572
    @test DHStorage.supplytemperature(-30.0; Ta0=5.0, Ta1=-30.0, Ts0=75.0, Ts1=120.0) == 120.0
    @test DHStorage.supplytemperature(-35.0; Ta0=5.0, Ta1=-30.0, Ts0=75.0, Ts1=120.0) == 120.0
end

@testset "Test model saving and loading" begin
    m = sample_model()
    m2 = sample_model()
    m3 = Model()
    file = tempname()
    rm(file, force=true) #HACK jldopen(tempname(), "a+") throws EOFerror
    h = hash(m)
    @testset "Hashes" begin
        @test h == 0x462204e3896fcde1
        @test h == hash(m2)
        @test h != hash(m3)
    end
    @testset "Model saving" begin
        @test_throws Exception save_solution(m, filename=file) # Unsolved model: error
        solve(m)
        @test_nowarn save_solution(m, filename=file)
        jfile = @test_nowarn jldopen(file, "r")
        @test haskey(jfile, repr(h))
        @test haskey(jfile[repr(h)], "colVal")
    end
    @testset "Model loading" begin
        @test_nowarn load_solution(m2, file)
        @test all(m.colVal .== m2.colVal)
        @test_throws Exception load_solution(m3, file) # No matching model
    end
end
